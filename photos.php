<?php include ("header.php"); ?>
<head>
	<link href="lightbox/dist/css/lightbox.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/photos.css">
	<link rel="stylesheet" type="text/css" href="css/friends.css">
</head>

	

<div class="container w-75 glavni">
<script type="text/javascript" src="js/photos.js"></script>

</div>


<div class="modal fade" id="addphotomodal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Ավելացնել նկար</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form action="server.php" method="post" enctype='multipart/form-data'>

	        <!-- Modal body -->
	        <div class="modal-body">
                 <input type="file" name="npht" accept="image/*" class="newimageinp" >
	        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" name="addphoto">Ավելացնել</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Փակել</button>
        </div>

        </form>
      </div>
    </div>
  </div>


<div class="container w-75 forphotos" style="position: relative;">

<h3 style="margin-top: 72px;">
		Նկարներ
</h3>
<button class="btn btn-outline-success" title="Ավելացնել նկար" style="position: absolute; right: 14px;top: 0" data-toggle="modal" data-target="#addphotomodal"><i class="fas fa-camera"></i></button>
<hr>
<div class="userphotosa w-100">
	
</div>


</div>

<script src="lightbox/dist/js/lightbox-plus-jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


	<?php 
		if($_SESSION['midatarktox'] == 2){ ?>
			
			<script type="text/javascript">
				   Swal.fire({
			        icon: 'error',
			        title: 'Oops...',
			        text: 'Նկար տեղադրված չկա',
			      })
			</script>

		<?php  } ?>

<?php include ("footer.php");?>