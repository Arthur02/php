<?php 
session_start();
// print_r ($_SESSION['photo']);
if (!isset($_SESSION['user'])){
        header("Location:index.php");
     
}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>QyuBy</title>
    <link rel="shortcut icon" type="image/x-icon" href="logo/logo1.jpg" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/nav.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossorigin="anonymous">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        
        <!-- <link rel="stylesheet" type="text/css" href="leftbar.css"> -->
</head>
<body>
<nav class="navbar navbar-expand-sm navig">
  <div class="container w-90 mx-auto">
  <a class="navbar-brand" href="profile.php">
    <img src="logo/logo1.jpg" alt="logo" width="40">
  </a>
  <div style="display: inline-block;" class="w-50 inos">
    <input class="form-control mr-sm-2 w-100 voronum" type="text" placeholder="Որոնում">
   <button class="btn srchbtn"><i class="fas fa-search"></i></button>
   <div class="search_res w-100" >

    </div>
  </div>
  <!-- <input type="text" name="" class="form-control w-50 navinp" placeholder="Search"> -->
  <ul class="navbar-nav" style="position: relative;">
    <li class="nav-item mr-2">
      <!-- <a class="nav-link nlink plink" href="profile.php"> -->
        <img class="rounded-circle myimage" width="33" src="<?= $_SESSION['user'][0]['photo']?> ">
      <!-- </a> -->
    </li>
    <li class="nav-item" style="position: relative;">
      <!-- Էս ծանուցումներնա -->
      <div style="position: relative" class="notification">
        <div class="badge" style="display: none">1</div>
        <a class="nav-link nlink text-light notifications" title="Ծանուցումներ" style="font-size: 17px"><i class="fas fa-bell"></i></a>
     
      </div>
    </li>
           <div class="openme" style="opacity: 1">
            
          </div>
    <li class="nav-item" style="position: relative;">
      <!-- Էս ծանուցումներնա -->
      <div style="position: relative" class="notification1">
        <div class="badge1 badge2" style="display:none">1</div>
        <a class="nav-link nlink text-light notifications1" title="Հաղորդագրություններ" style="font-size: 17px"><i class="fas fa-envelope"></i></a>
     
      </div>
    </li>
           <div class="openme1" style="opacity: 1">
            
          </div>
    <li class="nav-item">
      <a class="nav-link nlink" href="games.php" title="Խաղեր" style="font-size: 17px"><i class="fas fa-gamepad"></i></a>
    </li>
    <li class="nav-item">
      <a class="nav-link nlink" href="option.php" title="Կարգավորումներ" style="font-size: 17px"><i class="fas fa-cogs"></i></a>
    </li>
    <li class="nav-item">
      <a class="nav-link nlink" href="index.php" title="Դուրս գալ" style="font-size: 17px"><i class="fas fa-sign-out-alt"></i></a>
    </li>
  </ul>
  </div>
</nav>


<div class="messagebox">
  <div class="chatername">
    <p class="closeme">&times;</p>
  </div>
  <hr>
  <div class="mejinna">
    
  </div>
  <hr>
  <div class="stexgreq" style="position: relative;">
    <input type="text" name="" class="form-control grelu" style="border:none; width:87%;" placeholder="Գրեք ձեր հաղորդագրությունը">
    <button class="btn btn-success senda" style="position: absolute; right: 0; top: 0;"><i class="fas fa-paper-plane"></i></button>

  </div>
  
</div>


<script type="text/javascript" src="js/nav.js"></script>

	