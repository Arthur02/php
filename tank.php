<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="css/tank.css">
	<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
</head>
<body>
<audio id="myAudio" loop>
  <source src="Sounds/Cyberpunk.mp3" type="audio/mpeg">
</audio>
<audio id="tankAudio">
  <source src="Sounds/Tank Firing - Sound Effect (mp3cut.net).mp3" type="audio/mpeg">
</audio>
<audio id="suc">
	<source src="Sounds/Success-sound-effect.mp3">
</audio>
</div>
	<div id="game">
		<div class="achok">
		<h1 id="Score">Score:</h1>
		<h1 id="score">0</h1>
		</div>
		<img src="games/tank1.png" id="mytank">
		<button class="nomusic" title="On/Off Music"><i class='lni lni-music'></i></button>
	</div>
	<div class="startgame">
		<button class="info-btn" title="Info about game">&#9432;</button>
	<div class="info">
		<button class="close-info">&times;</button>
		<div class="info-header">
			<h1>Tank Battle Game</h1>
		</div>
		<div class="info-body">
			<p>Controls:</p>
			<p>Arrow Left - Left</p>
			<p>Arrow Right - Right</p>
			<p>Space - Fire</p>
		</div>
	</div>
			<button class="start">Start</button>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/tank.js"></script>
<?php include ('footer.php') ?>