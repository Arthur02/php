$(document).ready(function(){


	let arr = ["ani","hakob","anush","garnik","gurgen","nune","gevorg","anna","abo","krlo","grno","vahe","vardan","gugo","valentina","mari","mane","meri","dasha","masha"]
	$(".start").click(function(){
		$(this).fadeOut(1000, ()=>{
			$(this).remove()
		})
		$(".but").css({
			opacity: "1"
		})
	});
$(".Easy").click(function(){
		$(".but").css({
			opacity: "0",
			// display: "none",

		})
		let x = setInterval(function(){
			let r = rand(arr.length-1)
			let name = arr[r]
			$(`<div class="name">${name}</div>`).css({
				position: "absolute",
				top: "0",
				left: rand($(".game").width() - $(".name").width()-10),
				display: "inline-block",
				border: "2px solid white",
				borderRadius: rand(30) + "px",
				background: "rgba(0,0,0,.5)",
				width: "80px"
			}).appendTo('.game')
			$('.name').each(function(index, el) {
				$(this).animate({
					top: "+=20",
				})
				if ($(this).position().top+$(this).height() +30 >= $(".game").height()) {
					$('.game').css({
						zIndex: "300",
						background: "black",
						color: "white",
					}).html("Game Over")
					clearInterval(x)
					$("#txt").html("Press F5 to restart")
				}
				// console.log($(this).position().top)
			});
		},1500)
	})
	$(".Medium").click(function(){
		$(".but").css({
			opacity: "0",
			// display: "none",

		})
		$(".game").addClass('bgm')
		let x = setInterval(function(){
			let r = rand(arr.length-1)
			let name = arr[r]
			$(`<div class="name">${name}</div>`).css({
				position: "absolute",
				top: "0",
				left: rand($(".game").width() - $(".name").width()-10),
				display: "inline-block",
				border: "2px solid white",
				borderRadius: rand(30) + "px",
				background: "rgba(0,0,0,.5)",
				width: "80px"
			}).appendTo('.game')
			$('.name').each(function(index, el) {
				$(this).animate({
					top: "+=18",
				})
				$(".but").css({
					display: "none"
				})
				if ($(this).position().top+$(this).height() +30 >= $(".game").height()) {
					$('.game').css({
						zIndex: "300",
						background: "black",
						color: "white",
					}).html("Game Over")
					clearInterval(x)
					$("#txt").html("press F5 to restart")
				}
				// console.log($(this).position().top)
			});
		},1000)
	})
	$(".Hard").click(function(){
		$('body').css({color: "red"})
		$(".but").css({
			opacity: "0",
			// display: "none",
		})
		$(".game").addClass('bgh')
		let x = setInterval(function(){
			let r = rand(arr.length-1)
			let name = arr[r]
		let v =	$(`<div class="name">${name}</div>`).css({
				position: "absolute",
				top: "0",
				left: rand($(".game").width() - $(".name").width()-10),
				display: "inline-block",
				border: "2px solid red",
				borderRadius: rand(30) + "px",
				background: "rgba(0,0,0,.5)",
				width: "80px",
				color: "red"
			}).appendTo('.game')
			$('.name').each(function(index, el) {
				$(this).animate({
					top: "+=22",
				})
				$(".but").css({
					display: "none"
				})
				if ($(this).position().top+$(this).height() +30 >= $(".game").height()) {
					$('.game').css({
						zIndex: "300",
						background: "black",
						color: "Red",
					}).html("Game Over")
					$("#txt").html("Press F5 to restart")
					clearInterval(x)
				}
				// console.log($(this).position().top)
				if (miavor == 10) {
					$(".bgh").addClass('easteregg')
				}
				else{
					$(".bgh").removeClass('easteregg')
				}
			});
		},600)
	})



	let miavor = 0
	let txt = ""
	$(document).on('keydown',function(e){
		txt += e.key
		if (e.key == "Backspace") {
				txt = txt.substring(0,txt.length-10)
			}
		else if(e.key == "Delete"){
			txt =""
		}
		$("#txt").html(txt)
		$('.name').each(function(){
			if ($(this).html() == txt) {
				miavor++
				$("#miavor").html(miavor)
				txt = ""
				$(this).remove()
				return
			}
		});
	})








	function rand(a){
		return Math.floor(Math.random()*a)
	}
	function randcolor(){
		return `rgb(${rand(255)},${rand(255)},${rand(255)})`
	}
})

	let x = document.getElementById("myAudio"); 
	function playAudio(){
		x.play();
	}
	let y = document.getElementById("hardAudio");
	function veryhard(){
		x.pause()
		y.play()
	}
// event.stopPropagation() նենցա անում որ փառենթի իվենթը չաշխատի
