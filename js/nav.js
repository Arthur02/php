jQuery(document).ready(function($) {
	$('.myimage').click(function(event) {
		location.href = "profile.php";
	});
	$(document).on('input','.voronum',function(r){
		$('.search_res').css({
			opacity: '1',
		})
		let req = $('.voronum').val();
		$(".search").remove();
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"search", search: req},
			success: function(r){
				r = JSON.parse(r)
				r.forEach(function(e){
					$(`<div class="search form-control" style="height:auto;">
						<a href="userpage.php?id=${e.name}${e.id}" id="${e.id}" style="text-decoration:none" class="pasajir">
							<img src="${e.photo}" width="25px" class="rounded-circle">
							<h6 class="d-inline-block">${e.name} ${e.surname}</h6>
						</a>
						<button id="${e.id}" class="btn anhayt ${e.status} btn-success" style="position:absolute; right: 10px; height:30px; font-size:12px;">
						a
						</button>
						
					</div>`).appendTo('.search_res')
					if (e.status == 'zro') {
	
						$(".zro").removeClass('sent')
						$(".zro").addClass('btn-success')
						$(".zro").addClass('addfriend')
						$(".zro").attr('title',"Ուղարկել ընկերության հայտ")
						$('.addfriend').html('<i class="fas fa-user-plus"></i>')
					}
					else if(e.status == 'sent'){
						$('.sent').addClass('btn-danger')
						$('.sent').addClass('remove_request')
						$('.remove_request').html('<i class="fas fa-user-slash">ջնջել հայտը</i>')
					}
					else if(e.status == 'recive'){
						$('.recive').addClass('btn-info')
						$('.recive').addClass('savereq')
						$('.savereq').html('<i class="fas fa-user-check">Ընդունել հայտը</i>')
						$(".recive").before($(`<button class="btn btn-danger jnjich" id="${e.id}"><i class="fas">Հեռացնել հայտը</i></button>`))

					}
					else if(e.status == 'friend'){
						$('.friend').addClass('remove_friend')
						$('.friend').html('<i class="fas fa-user-slash">Ընկերներ</i>')
					}
				})
			}
		})
	})
	$(document).on('click', '.addfriend', function(event) {
		$(this).removeClass('btn-success')
		$(this).addClass('btn-danger')
		$(this).addClass('remove_request')
		$(this).html('<i class="fas fa-user-slash">ջնջել հայտը</i>') 
		let hisid = $(this).attr('id')
		// console.log('asd')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"addfriend",his_id:hisid}
		})
		// console.log(hisid)
	});
	$(document).on('click', '.remove_request', function(event) {
		$(this).removeClass('btn-danger')
		$(this).addClass('btn-success')
		$(this).addClass('addfriend')
		$(this).removeClass('remove_request')
		$(this).html('<i class="fas fa-user-plus"></i>') 
		let hisid = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"removereq",his_id:hisid}
		})
	});
	$(document).on('click', '.savereq', function(event) {
		$(this).removeClass('btn-info')
		$(this).addClass('btn-success')
		$(this).addClass('friend')
		$(this).parent().find(".jnjich").remove()
		// $(this).removeClass('remove_request')
		$(this).html('<i class="fas">Ընկերներ</i>') 
		let hisid = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"savefriend",his_id:hisid}
		})
	});
	$(document).on('mouseenter', '.friend', function(event) {
		$(this).removeClass('btn-success')
		$(this).addClass('btn-danger')
		$(this).html('<i class="fas">Ջնջել ընկերոջը</i>') 
	});
	$(document).on('mouseleave', '.friend', function(event) {
		$(this).addClass('btn-success')
		$(this).removeClass('btn-danger')
		// $(this).removeClass('btn-success')
		$(this).html('<i class="fas">Ընկերներ</i>') 
	});
	$(document).on('click', '.friend', function(event) {
		$(this).addClass('btn-success')
		$(this).removeClass('btn-danger')
		$(this).addClass('addfriend')
		$(this).removeClass('friend')
		$(this).html('<i class="fas fa-user-plus"></i>') 
		let hisid = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"removefriend",his_id:hisid}
		})
	});
	$(document).on('click', '.jnjich', function(event) {
		$(this).parent().find(".recive").remove()
		$(this).addClass('btn-success')
		$(this).removeClass('btn-danger')
		$(this).addClass('addfriend')
		$(this).removeClass('jnjich')
		$(this).css({position:"absolute", right:"10px", height:"30px", fontSize:"12px"})
		$(this).html('<i class="fas fa-user-plus"></i>') 
		let hisid = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"chndunel",his_id:hisid}
		})
	});
	$(document).click(function(event){
		// event.preventdefault()
		$('.search_res').css({
			opacity: '0',
		})
		$(".search").remove();
	})
	$(document).on('click', '.search_res', function(event) {
		event.stopPropagation()
	});


	$(document).on('click', '.pasajir', function(event) {
		let id=$(this).attr('id')
		console.log(id)
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"gnacinqhyur", id:id},
			success:function(r){
				console.log(r);
			}
		})
	});

	// SEARCH ----------------------------------------------------------------------
	// $('.srchbtn').click(function(){
		// location.href = "search.php";
	// })


	// Notifications ---------------------------------------------------------------

	// setInterval(function(){
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"getnotif"},
			success: function(r){
				if (r != ""){
					r = JSON.parse(r)
					let i = r.length
					$(".badge").css({display:"block",})
					$(".badge").html(r.length)
						$('.notification').click(function(event) {
							$(".notifications").addClass('opened')
							// $(".notifications").html('<i class="fas fa-envelope-open"></i>')
							$(".frres").remove();
							$(".openme").css({
								opacity:'1',
							})
							// r.forEach(function(e){
							// 	$(`<div class="form-control frres">
							// 		<img src="${e.photo}" class="rounded-circle" width="30px" style="vertical-align:middle">
							// 		<h6 style="display:inline-block">${e.name} ${e.surname}</h6>
							// 		<button id="${e.id}" class="btn btn-info yndunum yndunum1">Ընդունել հայտը</button>
							// 		<button id="${e.id}" class="btn btn-danger yndunum yndunum2">Հեռացնել հայտը</button>
							// 	</div>`).appendTo('.openme')

							// })
					});	
						$(document).on('click', '.yndunum', function(event) {
							i--
							event.stopPropagation()
							$(this).parent().remove()
							$(".badge").html(i)
						});
				}
				// console.log(r)
			}
		})

					// չստացված փորձ ծանուցումներ
						$('.notification').click(function(event) {
							$(".notifications").addClass('opened')
							// $(".notifications").html('<i class="fas fa-envelope-open"></i>')
							$(".frres").remove();
							$(".openme").css({
								opacity:'1',
							})
							$.ajax({
								type:"post",
								url:"server.php",
								data:{action:"notifname"},
								success: function(r){
									if (r != "") {
									r = JSON.parse(r)
									r.forEach(function(e){
									$(`<div class="form-control frres">
										<img src="${e.photo}" class="rounded-circle" width="30px" style="vertical-align:middle">
										<h6 style="display:inline-block">${e.name} ${e.surname}</h6>
										<button id="${e.id}" class="btn btn-info yndunum yndunum1">Ընդունել հայտը</button>
										<button id="${e.id}" class="btn btn-danger yndunum yndunum2">Հեռացնել հայտը</button>
									</div>`).appendTo('.openme')

									})
								}
								}
							})
					
					});	
	// },1000)


	$(document).click(function(event) {
		// console.log("asd")
		// event.preventdefault()
		$('.openme').css({
			opacity:'0',
		})
		$(".frres").remove();
		// $(".opened").html('<i class="fas fa-envelope"></i>')
	})
	$(document).on('click', '.openme', function(event) {
		// console.log("dsa")
		event.stopPropagation()
	});
	$(".notification").click(function(event) {
		// console.log("AAA")
	// 	/* Act on the event */
	// 	console.log("asdasd")
		event.stopPropagation()
		$('.openme').css({
			opacity:'0',
		})
	
	});



// --------------------------------message notif--------------------------------------------------
		let a = function(){
			setInterval(function(){

				$.ajax({
				type:"post",
				url:"server.php",
				data:{action:"getnotif1"},
				success: function(r){
					if (r != ""){
						r = JSON.parse(r)
					}
					else{
						$('.badge2').html(0)
					}
						let i = r.length
						$(".badge2").css({display:"block",})
						$(".badge2").html(r.length)
							$('.notification1').click(function(event) {
								$(".notifications1").addClass('opened1')
								$(".notifications1").html('<i class="fas fa-envelope-open"></i>')
								$(".frres").remove();
								$(".openme1").animate({
									opacity:'1',
								})
						});	
							console.log(i)
					// console.log(r)
				}
			})
		},1000)
	}
	a()
				let ghj = 0

					$('.notification1').click(function(event) {
						$(".pasdasd").remove()
							$(".notifications1").addClass('opened1')
							// $(".notifications").html('<i class="fas fa-envelope-open"></i>')
							$(".frres1").remove();
							$(".openme1").css({
								opacity:'1',
							})
							$.ajax({
								type:"post",
								url:"server.php",
								data:{action:"getmyfriends2"},
								success: function(r){
								if (r != "") {
									r = JSON.parse(r)
									console.log(r)
									r.forEach(function(e){
									if(e.meses != ""){
										// console.log('asd')
										ghj = e.meses[0].cmes
									}
									else{
										ghj = 0
									}
									$(`<div class="w-100 h-100 form-control pasdasd" style="position:relative">
										<a href="userpage.php?id=${e.name}${e.id}" id="${e.id}" style="text-decoration:none" class="friendsclick">
											<div style="background:url(${e.photo});" class="my_friends1 rounded-circle hisname">

											</div>
											<h6 class="hisname">${e.name} ${e.surname}</h6>
										</a>
											<button class="btn btn-info button_status1" title="chat" id="${e.id}"><i class="fas fa-envelope"></i></button>
											<a href="${e.bg_photo}" data-lightbox="image${e.id}" class="w-50 friendbg1" style="z-index:20" ">
												<div class="w-50 d-inline-block friendbg1">
												</div>
											</a>
											<div class="qanihat"><p class="hatik"><b>${ghj}</b></p></div>
											<div style="background:url(${e.bg_photo})" class="w-50 d-inline-block friendbg1">
											
											</div>
												</div>`).appendTo('.openme1')
									})
									}
								}
							})
					
					});	

					$(document).click(function(event) {
						// console.log("asd")
						// event.preventdefault()
						$('.openme1').css({
							opacity:'0',
						})
						$(".pasdasd").remove();
						$(".opened1").html('<i class="fas fa-envelope"></i>')
					})
					$(document).on('click', '.openme1', function(event) {
						// console.log("dsa")
						event.stopPropagation()
					});
					$(".notification1").click(function(event) {
						// console.log("AAA")
					// 	/* Act on the event */
					// 	console.log("asdasd")
						event.stopPropagation()
						$('.openme1').css({
							opacity:'0',
						})
					
	});




	let interval
		$(document).on('click', '.button_status1', function(event) {
			setTimeout(function(){
				$('.mejinna').animate({ scrollTop: $('.mejinna').prop('scrollHeight') })
			},1500)
			clearInterval(interval)
			$('.messagebox').animate({
				bottom:"0px",
			})
			$('.imchater').remove()
			let id = $(this).attr('id')
			// console.log(id)
			$.ajax({
				type:"post",
				url:"server.php",
				data:{action:"withhwo",id:id},
				success:function(r){
					r = JSON.parse(r)
					// console.log(r)
					$(`
						<div class="imchater">
							<img src="${r[0].photo}" width="35px" class="rounded-circle">
							<span> ${r[0].name} ${r[0].surname} </span>
						</div>

						`).appendTo('.chatername')

					$('.senda').attr('id', `${r[0]['id']}`)

				}
			})
				interval = setInterval(function(){
			$.ajax({
				type:"post",
				url:"server.php",
				data:{action:"chatinwithfriends",id:id},
				success:function(r){
					r = JSON.parse(r)
					$('.uxakimes').remove()
					console.log(r)
					r.forEach(function(e){
						$(`

						<div class="uxakimes" id="${e.user_2_id}" style="position:relative">
							<p>${e.message}</p>
							<p style="font-size:10px">${e.sent_at}</p>
							<span class="teselairany pspan" id="a${e.read}"><i class="fas fa-eye-slash"></i></span>
						</div>

							`).appendTo('.mejinna')
					})

					$('.uxakimes').each(function(el) {
		
						if($(this).attr('id') == id){
							$(this).addClass('mymessage')
							$(this).find('span').addClass('teselaimy')
							$(this).find('span').removeClass('teselairany')
						}
						else{
							$(this).addClass('hismessage')
						}
						
					});
					$('.pspan').each(function(ep){
						if($(this).attr('id') == 'a1'){
							$(this).html('<i class="fas fa-eye"></i>')
						}
					})

				}
			})
			

		},1000)
			

		});
		$(document).on('click', '.closeme', function(event) {
			$('.messagebox').animate({
				bottom:"-500px",
			})
			clearInterval(interval)
		});
// 		function (event) {
//     		if (event.which == 13 || event.keyCode == 13) {
//        		 // return false;
//     }
// };
		$(document).keydown(function(event){
			if (event.keyCode == 13){
				$('.senda').click()
			}
		})

		$('.senda').click(function(event) {
			let mes = $('.grelu').val()
			let hisid = $(this).attr('id')
			if(mes != ""){

				$.ajax({
					type:"post",
					url:"server.php",
					data:{action:"sending", hisid:hisid, mes:mes },
					// success:function	
				})
				$('.grelu').val("")
				$(`

					<div class="mymessage uxakimes">
						<p>${mes}</p>
					</div>


					`).appendTo('.mejinna')
			}
			setTimeout(function(){
				$('.mejinna').animate({ scrollTop: $('.mejinna').prop('scrollHeight') })
			},500)

		});
// ----------------------------------------------------------------------------------

	$(document).on('click', '.opened', function(event) {
		$(".opened").html('<i class="fas fa-envelope"></i>')
		
	});


	$(document).on('click', '.yndunum2', function(event) {
		let hisid = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"chndunel", his_id:hisid}
		})
		// $(this).parent().remove()
	});
	$(document).on('click', '.yndunum1', function(event) {
		let hisid = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"savefriend", his_id:hisid}
		})
		// $(this).parent().remove()
	});




		$.ajax({
		type:"post",
		url:"server.php",
		data:{action:"gethisstyle"},
		success:function(r){
			r = JSON.parse(r)
			// console.log(r[0]['userstyle'])
			if (r[0]['userstyle'] == 'dark') {
				$(".messagebox").css({
					background:'#292929',
					color:'white',
					border:"1px solid white"
				})
				// $('hr').attr('color', 'white')
				$(".grelu").css({
					background:""
				})
			}
			else if(r[0]['userstyle'] == 'gradient'){
				$('.chatername').css({
					background: "rgb(18,221,255)",
					background: "-webkit-linear-gradient(right, rgba(18,221,255,1) 0%, rgba(182,49,255,1) 48%, rgba(228,0,255,1) 86%)",
					background: "-o-linear-gradient(right, rgba(18,221,255,1) 0%, rgba(182,49,255,1) 48%, rgba(228,0,255,1) 86%)",
					background: "linear-gradient(to left, rgba(18,221,255,1) 0%, rgba(182,49,255,1) 48%, rgba(228,0,255,1) 86%)"
				})
				$('.grelu').css({
					background: "rgb(18,221,255)",
					background: "-webkit-linear-gradient(right, rgba(18,221,255,1) 0%, rgba(182,49,255,1) 48%, rgba(228,0,255,1) 86%)",
					background: "-o-linear-gradient(right, rgba(18,221,255,1) 0%, rgba(182,49,255,1) 48%, rgba(228,0,255,1) 86%)",
					background: "linear-gradient(to left, rgba(18,221,255,1) 0%, rgba(182,49,255,1) 48%, rgba(228,0,255,1) 86%)",
					color:"white",
				})
			}

		}
	})



});

