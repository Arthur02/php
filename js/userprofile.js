jQuery(document).ready(function($) {
	
	let id = $(".clikid").attr('id')
	let myid = $(".myid").attr('id')
		$.ajax({
		url: 'server.php',
		type: 'post',
		data: {action: 'profileinfo', id:id},
		success: function(r){
			r = JSON.parse(r);
			// console.log(r[0]['photo']);
			let photo = r[0]['photo'];
			let bgph = r[0]['bg_photo'];
			let name = r[0]['name']
			let surname = r[0]['surname']
			let age = r[0]['age']
						$(`
					<div class="userinfo">
						<div class='userphotos'>
							<div class="usersback">
								<a href="${bgph}" data-lightbox="image-1">
									<div class="motik">
									</div>
								</a>
									<div style="background-image: url(${bgph})" class="profbg">
										<button class="btn btn-info friend_status" style="position:absolute; bottom:10px; right:10px">Ընկերներ</button>
									</div>
							<a href="${photo}" data-lightbox="image-2">
								<div style="background-image: url(${photo})" class="rounded-circle usimg">
								</div>
							</a>
						</div>
						<div class="alluserinfo">
							<h3 class="username">${name} ${surname}<h3>
							<h6 class="username">${age} Տարեկան <h6>
						</div>
					</div>
					`).appendTo('.glavni')
			$(".postt").ready(function(){
				if (r[0]['userstyle'] == 'dark') {
					$('body').css({
						background:"#181512",
						color:"white"
					})
					$(".modal-content").css({
						background:"#181512"
					})
					$('.usernews').css({
						color:"white",
						background:"#292929",
						border:"1px solid white"
					})
					$('.newstext').css({
						color:'white',
					})
					$(".delaypost").css({
						background:"#292929",
					})
					$(".sidebar-images").css({
						background:"#292929",
						border:"1px solid white"
					})
					$(".sidebar-friends").css({
						background:"#292929",
						border:"1px solid white"
					})
					$(".newstext").css({
						color:"white",
					})
					$(".frannun").css({
						color:"white",
					})
				}
				else if(r[0]['userstyle'] == 'gradient'){
					$('body').css({
						background: "rgb(18,221,255)",
						background: "linear-gradient(180deg, rgba(18,221,255,1) 0%, rgba(228,0,255,1) 60%, rgba(228,0,255,1) 100%)",
						backgroundAttachment:"fixed",
						color:"white"
					})
					$(".modal-content").css({
						background: "rgb(18,221,255)",
						background: "linear-gradient(180deg, rgba(18,221,255,1) 0%, rgba(228,0,255,1) 60%, rgba(228,0,255,1) 100%)",
					})
					$('.usernews').css({
						color:"black",
					})
				}

			})

		}
	})
	$('.postt').ready(function(){
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"friendstat",id:id},
			success:function(r){
				console.log(r)
					if (r == 'Ուղարկել հայտ') {
						$(".friend_status").addClass('paddfriend')
						$(".friend_status").removeClass('sent')
						// $(".friend_status").addClass('btn-success')
						// $(".friend_status").attr('title',"Ուղարկել ընկերության հայտ")
						$('.paddfriend').html('Ուղարկել հայտ')
					}
					else if(r == 'Ջնջել հայտը'){
						$('.friend_status').addClass('btn-danger')
						$('.friend_status').addClass('premove_request')
						$('.premove_request').html('ջնջել հայտը')
					}
					else if(r == 'Ընդունել հայտը'){
						$('.friend_status').addClass('btn-info')
						$('.friend_status').addClass('psavereq')
						$('.psavereq').html('Ընդունել հայտը')
						$(".friend_status").before(`<button class="btn btn-danger pjnjich" id="${id}">Հեռացնել հայտը</button>`)

					}
					else if(r == 'ընկերներ'){
						$('.friend_status').addClass('pfriend')
						$('.friend_status').html('Ընկերներ')
					}
			}
		})

	})

	$(document).on('click', '.paddfriend', function(event) {
		console.log(id + 'addfr')
		// $(this).removeClass('btn-success')
		$(this).removeClass('paddfriend')
		$(this).addClass('btn-danger')
		$(this).addClass('premove_request')
		$(this).html('ջնջել հայտը') 
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"addfriend",his_id:id}
		})
	});
	$(document).on('click', '.premove_request', function(event) {
		$(this).removeClass('btn-danger')
		$(this).addClass('btn-success')
		$(this).addClass('paddfriend')
		$(this).removeClass('premove_request')
		$(this).html('Ուղարկել հայտ') 
		console.log(id + 'remreq')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"removereq",his_id:id}
		})
	});
	$(document).on('click', '.psavereq', function(event) {
		$(this).removeClass('btn-info')
		$(this).addClass('btn-info')
		$(this).addClass('pfriend')
		$(this).parent().find(".pjnjich").remove()
		// $(this).removeClass('premove_request')
		$(this).html('Ընկերներ') 
		console.log(id + 'psavereq')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"savefriend",his_id:id}
		})
	});
	$(document).on('mouseenter', '.pfriend', function(event) {
		$(this).removeClass('btn-success')
		$(this).addClass('btn-danger')
		$(this).html('Ջնջել ընկերոջը') 
	});
	$(document).on('mouseleave', '.pfriend', function(event) {
		$(this).addClass('btn-success')
		$(this).removeClass('btn-danger')
		// $(this).removeClass('btn-success')
		$(this).html('Ընկերներ') 
	});
	$(document).on('click', '.pfriend', function(event) {
		$(this).addClass('btn-success')
		$(this).removeClass('btn-danger')
		$(this).addClass('paddfriend')
		$(this).removeClass('pfriend')
		$(this).html('Ուղարկել հայտ') 
		console.log(id + 'pfriend')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"removefriend",his_id:id}
		})
	});
	$(document).on('click', '.pjnjich', function(event) {
		$(this).parent().find(".psavereq").remove()
		$(this).addClass('btn-info')
		$(this).removeClass('btn-danger')
		$(this).addClass('paddfriend')
		$(this).removeClass('pjnjich')
		$(this).css({position:"absolute", right:"10px", bottom:"10px"})
		$(this).html('Ուղարկել հայտ')
		console.log('jnjich')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"chndunel",his_id:id}
		})
	});

		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"userposts",id:id},
			success:function(r){
				r = JSON.parse(r);
				// console.log(r);
					r.forEach(function(e){
						let p = 0
							if(e.likes != ''){
								p = e.likes[0]['clikes'];
							}
					$(`
						<div class="p-2 postt">
							<a href ="userpage.php?name=${e.name}" style="color:black; text-decoration:none;">
								<img src="${e.photo}" width="40px" class="rounded-circle">
								<h5 style="display:inline-block" class="newstext">${e.name} ${e.surname}</h5>
							</a>
							<p style="color:black; margin-left:45px; font-size:10px" class="newstext">${e.esjam}</p>
							<h6 class="mt-3" style="word-break: break-word;">${e.status}</h6>
								<img src="${e.hasce}" style="max-width:100%; min-width:70%;">
							<hr>
								<div style="height:20px; color:#3f5efb; display:inline-block; cursor:pointer" class="newstext chav" id="${e.id}" data-toggle="modal" data-target="#likesopen">
									Հավանումներ:
									<p style="display:inline-block" class="countlikes newstext" data-id="${e.id}">
										${p}
									</p>
								</div>
							<hr>
							<div>
								<button class="btn knopkeq mt-1 mb-1 like" id="${e.id}">
									<i class="fas fa-heart"></i> Հրաշալի
								</button>
								<button class="btn knopkeq mt-1 mb-1 comm" id="${e.id}">
									<i class="fas fa-comment"></i> Մեկնաբանել
								</button>
								<button class="btn knopkeq mt-1 mb-1 share" id="${e.id}">
									<i class="fas fa-share-square"></i> Կիսվել
								</button>

							</div>
							<hr>
							<div class="mycoms">
								<div class="writencoms">
							</div>
								<div style="position:relative; width:100%;">
									<textarea class="form-control pcomentstext" style="resize:none; height:38px" placeholder="Ի՞նչ ես կարծում"></textarea>
									<button class="btn btn-outline-info psendcom" id="${e.id}">Ուղարկել</button>
								</div>
					<hr>
							</div>
					

						</div>
						`).appendTo('.usernews')
				})
			}
		})


			let i
			let j


			$.ajax({
				type:"post",
				url:"server.php",
				data:{action:"gethisfriends",id:id},
				success:function(r){
					// console.log(r)
					r = JSON.parse(r)
					r.forEach(function(e){
						
						$(`
							<div class="left-friends p-1 mt-1" id="${e.id}">
							<a href="userpage.php?name=${e.name}" style="color:#495057; text-decoration:none" class="hxum">
								<div style="background:url(${e.photo})" class="lffr rounded-circle">
								</div>
								<h6 align="center" class="frannun">${e.name} ${e.surname}</h6>
								</a>
							</div>
							`).appendTo('.sidebar-friendsi')

					})
						// console.log(r)
				}
			})
			$.ajax({
				type:"post",
				url:"server.php",
				data:{action:"gethisphotos",id:id},
				success:function(r){
					// console.log(r)
						r = JSON.parse(r)
						r.forEach(function(e){		
				$(`
					<div class="left-images p-1 mt-1" style="background: url(${e.hasce});">
						<a href="${e.hasce}" data-lightbox="roadtrip">
							<div class="leftmotik p-1 mt-1">
							</div>
						</a>
					</div>`).appendTo('.sidebar-imagesi')
				})
			}
		})
	$(document).on('click', '.left-friends', function(event) {
		let id = $(this).attr('id')
		if(id != myid ){
			$.ajax({
				type:"post",
				url:"server.php",
				data:{action:"gnacinqhyur", id:id},
				success:function(r){
					// console.log(r)
				}
			})
		}
		else{
			$(".hxum").attr('href', 'profile.php')
			// console.log('asd')
		}
	});


	$('.postt').ready(function(){
			setTimeout(function(e){
				 // i = $('.sidebar-images').offset().top - 72
				 j = $('.sidebar-friends').offset().top - 72

			},30)
			// if(j > 1328)
			// scroll նա
			// if($('html').height() > 1390){
			$(window).on('scroll', function(event) {
				// console.log($('body').height())
				// console.log(a)
				// if($('body').height() > 1330)
				if($(this).scrollTop() >= j){
					$('.sidebar-friends').addClass('addclassf');
				}
				else{	
					$('.sidebar-friends').removeClass('addclassf');
				}
			})
		// }
		$(document).on('click', '.like', function(event) {
			let statid = $(this).attr('id')
			let a = $(this).parents(".postt").find(".countlikes").html()
			$(this).parents(".postt").find(".countlikes").html(++a)
			console.log(statid)
			$.ajax({
				type:"post",
				url:"server.php",
				data:{action:"likel", nkid:statid},
				success: function(r){
					console.log(r)
				}
			})
			$(".countlikes").html()
			$(this).removeClass('like')
			$(this).addClass('unlike')
			// $(this).html()
			
		});
		$(document).on('click', '.unlike', function(event) {
			// event.preventDefault();
			let a = $(this).parents(".postt").find(".countlikes").html()
			$(this).parents(".postt").find(".countlikes").html(--a)
			let statid = $(this).attr('id')
			$(this).removeClass('unlike')
			$(this).addClass('like')
			/* Act on the event */		
			$.ajax({
				type:"post",
				url:"server.php",
				data:{action:"unlikel", nkid:statid},
				success: function(r){
					console.log(r)
				}
			})
		});
		
	$.ajax({
		type:"post",
		url:"server.php",
		data:{action:"helikedme"},
		success:function(r){
			if (r != "" ) {
				r = JSON.parse(r)
					r.forEach(function(r){
						$(".like").each(function(e){
							if ($(this).attr('id') == r.photo_id) {
								// console.log('asd')
								$(this).addClass('unlike')
								$(this).removeClass('like')
							}
						})

						// console.log(r.photo_id);
					})
				
			}
		}
	})
	})


	$('.hissfr').click(function(r){
		$('.asdasd').remove()
		let hisid = id;
		$.ajax({
			type:"post",
			url:'server.php',
			data:{action:'imgonagetyou',id:hisid},
			success:function(r){
				r = JSON.parse(r)
				console.log(r)
							r.forEach(function(e){
				
				$(`
					<div class="w-100 h-100 form-control asdasd">
						<a href="userpage.php?id=${e.name}${e.id}" id="${e.id}" style="text-decoration:none" class="friendsclick">
							<div style="background:url(${e.photo});" class="my_friends rounded-circle hisname">

							</div>
							<h6 class="hisname">${e.name} ${e.surname}</h6>
						</a>
							<a href="${e.bg_photo}" data-lightbox="roadtrip" class="w-50 friendbg" style="z-index:20" ">
								<div class="w-50 d-inline-block friendbg">
								</div>
							</a>
							<div style="background:url(${e.bg_photo})" class="w-50 d-inline-block friendbg">
							
							</div>
					</div>
					`).appendTo('.mb1')

			})
			}
		})
	})

	$('.hissph').click(function(event) {
		let hisid = id;
		$('.thephotos').remove()
		$.ajax({
			type:"post",
			url:'server.php',
			data:{action:'imgonagetphot',id:hisid},
			success:function(r){
				r = JSON.parse(r)
				console.log(r)
				r.forEach(function(r){
					$(`
						<div class="d-inline-block m-1 thephotos" style="width:31.333%;">
						<a href="${r.hasce}" data-lightbox="roadtrip" class="w-100">
							<img src="${r.hasce}" class="w-100" style="object-fit: cover; height:316px;" ">
						</a>
						</div>

						`).appendTo('.mb2')
				})

			}
		})
	});

		$(document).on('click', '.friendsclick', function(event) {
			let id = $(this).attr('id')
			if (id != myid) {
			$.ajax({
				type:"post",
				url:"server.php",
				data:{action:"gnacinqhyur", id:id},
				success:function(r){
					console.log(r)
				}
			})
		}
		else{
			$(this).attr('href', 'profile.php')
		}
	});


$(document).on('click', '.chav', function(event) {
	let id = $(this).attr('id')
	console.log(id)
	$.ajax({
		type:"post",
		url:"server.php",
		data:{action:"hwoliked", postid:id},
		success:function(r){
			$(".asdasd").remove()
			r = JSON.parse(r)
			console.log(r)
			if(r != ""){

				r.forEach(function(e){
					
					$(`
						<div class="w-100 h-100 form-control asdasd">
							<a href="userpage.php?id=${e.name}${e.user_id}" id="${e.user_id}" style="text-decoration:none" class="friendsclick">
								<div style="background:url(${e.photo});" class="my_friends rounded-circle hisname">

								</div>
								<h6 class="hisname">${e.name} ${e.surname}</h6>
							</a>
								<a href="${e.bg_photo}" data-lightbox="roadtrip" class="w-50 friendbg" style="z-index:20" ">
									<div class="w-50 d-inline-block friendbg">
									</div>
								</a>
								<div style="background:url(${e.bg_photo})" class="w-50 d-inline-block friendbg">
								
								</div>
						</div>
						`).appendTo('.mb4')

				})
			}
			else{
				$(`<h5 class="asdasd">Դեռ հավանումներ չկան</h5>`).appendTo('.mb4')
			}
		}
	})
});


	$(document).on('click', '.comm', function(event) {
		$('.mycoms').css({
			display:"none",
		})
		$(this).parents('.postt').find('.mycoms').css({
			display:'block',
		})
		$('.hewrite').remove()
		let id = $(this).attr('id')
		let put = $(this).parents('.postt').find('.writencoms')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:'hwocomed',postid:id},
			success:function(r){
				r = JSON.parse(r)
				console.log(r)
				r.forEach(function(e){
					$(`

						<div class="hewrite mt-2 mb-2" style="border-radius:20px; border:1px solid white">
							<img src ="${e.photo}" width="30px" class="ml-1 mt-1 rounded-circle">
							<p style="display:inline-block; font-size:18px;">${e.name} ${e.surname}</p>
							<p style=" display:inline-block">${e.comment}</p><br>
							<p style="margin-left:45px; font-size:10px;">${e.time}</p>
						</div>



					`).appendTo(put)
				})
			}
		})
	});

	$(document).on('click', '.psendcom', function(event) {
		let hiscom = $(this).parent().find('.pcomentstext').val()
		let put = $(this).parents('.postt').find('.writencoms')
		let postid = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"commenting",photoid: postid, commyent:hiscom},
			success:function(r){	
				r = JSON.parse(r)
				console.log(r)
				$(`

						<div class="hewrite mt-2 mb-2" style="border-radius:20px; border:1px solid white">
							<img src ="${r[0].photo}" width="30px" class="ml-1 mt-1 rounded-circle">
							<p style="display:inline-block; font-size:18px;">${r[0].name} ${r[0].surname}</p>
							<p style=" display:inline-block">${hiscom}</p><br>
							<p style="margin-left:45px; font-size:10px;">Հենց նոր</p>
						</div>



					`).appendTo(put)
				// console.log(hiscom)

			}
		})
				$(this).parent().find('.pcomentstext').val("")
		// console.log(postid)
	});



	// ----------------------------------------------------------Share----------------------------------------------------

	$(document).on('click', '.share', function(event) {
		let post = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"sharing",post:post}
		})
		Swal.fire(
		  'Good job!',
		  'Դուք կիսվել եք post ով!',
		  'success'
		)
	});

});