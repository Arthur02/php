jQuery(document).ready(function($) {
	let myid = $(".myid").attr('id')
	$.ajax({
		url: 'server.php',
		type: 'post',
		data: {action: 'getInfo'},
		success: function(r){
			r = JSON.parse(r);
			// console.log(r[0]['photo']);
			let photo = r[0]['photo'];
			let bgph = r[0]['bg_photo'];
			let name = r[0]['name']
			let surname = r[0]['surname']
			let age = r[0]['age']
						$(`
					<div class="userinfo">
						<div class='userphotos'>
							<div class="usersback">
								<a href="${bgph}" data-lightbox="image-1">
									<div class="motik">
									</div>
								</a>
									<div style="background-image: url(${bgph})" class="profbg">
										<button style="z-index:98" title="change" type="button" class="btn btn-primary changebg" data-toggle="modal" data-target="#myModal" id="changebg">
 											<i class="fa fa-camera" aria-hidden="true"></i>
										</button>
										<button style="z-index:98" title="change style" type="button" class="btn text-light changestyle" data-toggle="modal" data-target="#mystyle">
											<i class="far fa-square"></i>
										</button>
									</div>
							<div style="background-image: url(${photo})" class="rounded-circle usimg">
								<button class="imgonachange rounded-circle usimg btn" data-toggle="modal" data-target="#userimage"> 
									<i class="fa fa-camera" aria-hidden="true"></i>
								</button>
							</div>
						</div>
						<div class="alluserinfo">
							<h3 class="username">${name} ${surname}<h3>
							<h6 class="username">${age} Տարեկան <h6>
						</div>
					</div>
					`).appendTo('.glavni')

		}
	})

	$(document).on('click', '#changebg', function(event) {
		
		event.preventDefault();
		event.stopPropagation();
		/* Act on the event */
	});
	$.ajax({
		type:"post",
		url:'server.php',
		data:{action: 'leftSidePhotos'},
		success: function(r){
			r = JSON.parse(r)
			
			r.forEach(function(e){
								
				$(`
					<div class="left-images p-1 mt-1" style="background: url(${e.hasce});">
						<a href="${e.hasce}" data-lightbox="roadtrip">
							<div class="leftmotik p-1 mt-1">
							</div>
						</a>
					</div>`).appendTo('.sidebar-imagesi')
				// 
			})
		}
	})


	let p

	$.ajax({
		type:"post",
		url:"server.php",
		data:{action:'usernews'},
		success: function(r){
			r = JSON.parse(r);
			console.log(r)
			
			r.forEach(function(e){
			let p = 0
				if(e.likes != ''){
					p = e.likes[0]['clikes'];
				}
				// console.log(e.clikes)
				$(`
					<div class="p-2 postt" style="position:relative">
						<a href ="profile.php" style="color:black; text-decoration:none;">
							<img src="${e.photo}" width="40px" class="rounded-circle" style="position:absolute">
							<h5 style="display:inline-block; margin-left:45px" class="newstext">${e.name} ${e.surname}</h5></a>
							<h3 style="display:inline-block; position:absolute; right:15px; cursor:pointer" class="clickfordel">...</h3>
							<div class="removemypost form-control" id="${e.id}">Ջնջել Պոստը</div>
							<p style="color:black; margin-left:45px; font-size:10px" class="newstext">${e.esjam}</p>
						<h6 class="mt-3 " style="word-break: break-word;">${e.status}</h6>
							<a href="${e.hasce}" data-lightbox="${e.id}">
								<img src="${e.hasce}" style="max-width:100%; min-width:70%;">
							</a>
						<hr>
							<div style="height:20px; color:#3f5efb; display:inline-block" class="newstext chav" id="${e.id}" data-toggle="modal" data-target="#likesopen">
								Հավանումներ:
								<p style="display:inline-block" class="countlikes newstext" data-id="${e.id}">
									${p}
								</p>
							</div>
						<hr>
						<div>
							<button class="btn knopkeq mt-1 mb-1 like" id="${e.id}">
								<i class="fas fa-heart"></i> Հրաշալի
							</button>
							<button class="btn knopkeq mt-1 mb-1 comm" id="${e.id}">
								<i class="fas fa-comment"></i> Մեկնաբանել
							</button>
							

						</div>
						<hr>
						<div class="mycoms">
							<div class="writencoms">
							</div>
								<div style="position:relative; width:100%;">
									<img src="${e.photo}" width="38px" class="rounded-circle">
									<textarea class="form-control comentstext" style="resize:none; height:38px" placeholder="Ի՞նչ ես կարծում"></textarea>
									<button class="btn sendcom" id="${e.id}">Ուղարկել</button>
								</div>
					<hr>
							</div>
						</div>
					`).appendTo('.usernews')
				console.log(p)
			})
		}
	})
	$.ajax({
		type:"post",
		url:"server.php",
		data:{action:"getmyfriends"},
		success:function(r){
			r = JSON.parse(r)
			// console.log(r);
			r.forEach(function(e){
				
				$(`
					<div class="left-friends p-1 mt-1" id="${e.id}">
					<a href="userpage.php?id=${e.id}" style="color:#495057; text-decoration:none">
						<div style="background:url(${e.photo})" class="lffr rounded-circle">
						</div>
						<h6 align="center" class="frannun">${e.name} ${e.surname}</h6>
						</a>
					</div>
					`).appendTo('.sidebar-friendsi')

			})
				console.log(r)
		}
	})
	let i
	let j
$(".postt").ready(function(){
	setTimeout(function(e){
		 // i = $('.sidebar-images').offset().top - 72
		 j = $('.sidebar-friends').offset().top - 72

	},30)
	// scroll նա
	$(window).on('scroll', function(event) {
		// console.log(a)
		if($(this).scrollTop() >= j){
			$('.sidebar-friends').addClass('addclassf');
		}
		else{	
			$('.sidebar-friends').removeClass('addclassf');
		}
	})

	$(document).on('click', '.like', function(event) {
		let statid = $(this).attr('id')
		let a = $(this).parents(".postt").find(".countlikes").html()
		$(this).parents(".postt").find(".countlikes").html(++a)
		console.log(a)
		console.log(statid)
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"likel", nkid:statid},
			success: function(r){
				console.log(r)
			}
		})
		$(".countlikes").html()
		$(this).removeClass('like')
		$(this).addClass('unlike')
		// $(this).html()
		
	});
	$(document).on('click', '.unlike', function(event) {
		let a = $(this).parents(".postt").find(".countlikes").html()
		$(this).parents(".postt").find(".countlikes").html(--a)
		// event.preventDefault();
		let statid = $(this).attr('id')
		$(this).removeClass('unlike')
		$(this).addClass('like')
		/* Act on the event */		
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"unlikel", nkid:statid},
			success: function(r){
				console.log(r)
			}
		})
})
	$.ajax({
		type:"post",
		url:"server.php",
		data:{action:"helikedme"},
		success:function(r){
			if (r != "" ) {
				r = JSON.parse(r)
					r.forEach(function(r){
						$(".like").each(function(e){
							if ($(this).attr('id') == r.photo_id) {
								// console.log('asd')
								$(this).addClass('unlike')
								$(this).removeClass('like')
							}
						})

						// console.log(r.photo_id);
					})
				
			}
		}
	})
		$.ajax({
		type:"post",
		url:"server.php",
		data:{action:"gethisstyle"},
		success:function(r){
			r = JSON.parse(r)
			console.log(r[0]['userstyle'])
			if (r[0]['userstyle'] == 'dark') {
				$('body').css({
					background:"#181512",
					color:"white"
				})
				$(".modal-content").css({
					background:"#181512"
				})
				$('.usernews').css({
					color:"white",
					background:"#292929",
					border:"1px solid white"
				})
				$('.newstext').css({
					color:'white',
				})
				$(".delaypost").css({
					background:"#292929",
				})
				$(".sidebar-images").css({
					background:"#292929",
					border:"1px solid white"
				})
				$(".sidebar-friends").css({
					background:"#292929",
					border:"1px solid white"
				})
				$(".newstext").css({
					color:"white",
				})
				$(".frannun").css({
					color:"white",
				})
			}
			else if(r[0]['userstyle'] == 'gradient'){
				$('body').css({
					background: "rgb(18,221,255)",
					background: "linear-gradient(180deg, rgba(18,221,255,1) 0%, rgba(228,0,255,1) 60%, rgba(228,0,255,1) 100%)",
					color:"white",
					// height:"100%",
					backgroundAttachment:"fixed",
				})
				$(".modal-content").css({
					background: "rgb(18,221,255)",
					background: "linear-gradient(180deg, rgba(18,221,255,1) 0%, rgba(228,0,255,1) 60%, rgba(228,0,255,1) 100%)",
				})
				$('.usernews').css({
					color:"black",
				})
			}

		}
	})
	$(document).on('click', '.comm', function(event) {
		$('.mycoms').css({
			display:"none",
		})
		$(this).parents('.postt').find('.mycoms').css({
			display:'block',
		})
		$('.hewrite').remove()
		let id = $(this).attr('id')
		let put = $(this).parents('.postt').find('.writencoms')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:'hwocomed',postid:id},
			success:function(r){
				r = JSON.parse(r)
				console.log(r)
				r.forEach(function(e){
					$(`

						<div class="hewrite mt-2 mb-2" style="border-radius:20px; border:1px solid white">
							<img src ="${e.photo}" width="30px" class="ml-1 mt-1 rounded-circle">
							<p style="display:inline-block; font-size:18px;">${e.name} ${e.surname}</p>
							<p style=" display:inline-block">${e.comment}</p><br>
							<p style="margin-left:45px; font-size:10px;">${e.time}</p>
						</div>



					`).appendTo(put)
				})
			}
		})
	});

	$(document).on('click', '.sendcom', function(event) {
		let hiscom = $(this).parent().find('.comentstext').val()
		let put = $(this).parents('.postt').find('.writencoms')
		let postid = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"commenting",photoid: postid, commyent:hiscom},
			success:function(r){	
				r = JSON.parse(r)
				console.log(r)
				$(`

						<div class="hewrite mt-2 mb-2" style="border-radius:20px; border:1px solid white">
							<img src ="${r[0].photo}" width="30px" class="ml-1 mt-1">
							<p style="display:inline-block; font-size:18px;">${r[0].name} ${r[0].surname}</p>
							<p style=" display:inline-block">${hiscom}</p><br>
							<p style="margin-left:45px; font-size:10px;">Հենց նոր</p>
						</div>



					`).appendTo(put)
				// console.log(hiscom)

			}
		})
				$(this).parent().find('.comentstext').val("")
		// console.log(postid)
	});
	});

	$(document).on('click', '.left-friends', function(event) {
		let id = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"gnacinqhyur", id:id},
			success:function(r){
				console.log(r)
			}
		})
	});

	$(document).on('click', '.colors', function(event) {
		let guyn = $(this).attr('id')
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:'changecolor',guyn:guyn}
		})
	});

	$(document).on('click', '.white', function(event) {
		$('body').css({
			background:'#EFEFEF',
			color:"black"

		})
		$(".modal-content").css({
			background:'#EFEFEF',
		})
		$('.usernews').css({
			color:"black",
			background:"white",
		})
		$('.newstext').css({
			color:'white',
		})
		$(".delaypost").css({
			background:"white",
		})
		$(".sidebar-images").css({
			background:"white",
		})
		$(".sidebar-friends").css({
			background:"white",
		})
		$(".newstext").css({
			color:"black",
		})
		$(".frannun").css({
			color:"black",
		})
	});
	$(document).on('click', '.dark', function(event) {
		$('body').css({
			background:"#181512",
			color:"white"
		})
		$(".modal-content").css({
			background:"#181512"
		})
		$('.usernews').css({
			color:"white",
			background:"#292929",
		})
		$('.newstext').css({
			color:'white',
		})
		$(".delaypost").css({
			background:"#292929",
		})
		$(".sidebar-images").css({
			background:"#292929",
		})
		$(".sidebar-friends").css({
			background:"#292929",
		})
		$(".newstext").css({
			color:"white",
		})
		$(".frannun").css({
			color:"white",
		})
	});
	$(document).on('click', '.gradient', function(event) {
		$('body').css({
			background: "rgb(18,221,255)",
			background: "linear-gradient(180deg, rgba(18,221,255,1) 0%, rgba(228,0,255,1) 60%, rgba(228,0,255,1) 100%)",
			color:"white",
			backgroundAttachment:"fixed"
		})
		$(".modal-content").css({
			background: "rgb(18,221,255)",
			background: "linear-gradient(180deg, rgba(18,221,255,1) 0%, rgba(228,0,255,1) 60%, rgba(228,0,255,1) 100%)",
		})
		$('.usernews').css({
			color:"black",
		})
		$('.usernews').css({
			color:"black",
			background:"white",
		})
		$('.newstext').css({
			color:'white',
		})
		$(".delaypost").css({
			background:"white",
		})
		$(".sidebar-images").css({
			background:"white",
		})
		$(".sidebar-friends").css({
			background:"white",
		})
		$(".newstext").css({
			color:"black",
		})
		$(".frannun").css({
			color:"black",
		})

	});

$(document).click(function(){
	$(".removemypost").css({
		display:'none'
	})
})
$(document).on('click', '.clickfordel', function(event) {
	console.log('asds')
	$(this).parents('.postt').find('.removemypost').css({
		display:'block'
	})
	event.stopPropagation()
});
$(document).on('click', '.removemypost', function(event) {
	let id = $(this).attr('id')
	console.log(id)
	event.stopPropagation()
	$.ajax({
		type:"post",
		url:'server.php',
		data:{action:'deletpost', id:id}
	})
	$(this).parents('.postt').remove()
});
$(document).on('click', '.chav', function(event) {
	let id = $(this).attr('id')
	console.log(id)
	$.ajax({
		type:"post",
		url:"server.php",
		data:{action:"hwoliked", postid:id},
		success:function(r){
			$(".asdasd").remove()
			r = JSON.parse(r)
			console.log(r)
			if(r != ""){

				r.forEach(function(e){
					
					$(`
						<div class="w-100 h-100 form-control asdasd">
							<a href="userpage.php?id=${e.name}${e.user_id}" id="${e.user_id}" style="text-decoration:none" class="friendsclick">
								<div style="background:url(${e.photo});" class="my_friends rounded-circle hisname">

								</div>
								<h6 class="hisname">${e.name} ${e.surname}</h6>
							</a>
								<a href="${e.bg_photo}" data-lightbox="roadtrip" class="w-50 friendbg" style="z-index:20" ">
									<div class="w-50 d-inline-block friendbg">
									</div>
								</a>
								<div style="background:url(${e.bg_photo})" class="w-50 d-inline-block friendbg">
								
								</div>
						</div>
						`).appendTo('.mb4')

				})
			}
			else{
				$(`<h5 class="asdasd">Դեռ հավանումներ չկան</h5>`).appendTo('.mb4')
			}
		}
	})
});
	// $('.addNew').click(function(){
		
	// 	let stat = $('.stat').val()
	// 	let image = $('.newimage').val();
	// 	// $.ajax({
	// 		// type:"post",
	// 		// url:"server.php",
	// 		// data:{action:'addNew', statname:stat, newimage:}
	// 	// })
	// 	// console.log(image)
	// })
	console.log(myid)
		$(document).on('click', '.friendsclick', function(event) {
			let id = $(this).attr('id')
			if (id != myid) {
			$.ajax({
				type:"post",
				url:"server.php",
				data:{action:"gnacinqhyur", id:id},
				success:function(r){
					console.log(r)
				}
			})
		}
		else{
			$(this).attr('href', 'profile.php')
		}
	});

// ---------------------------------------------------Comments-------------------------------------------------------------------------------



$('.addNew').click(function(){
	
})



});
