jQuery(document).ready(function($) {
	$.ajax({
		type: "post",
		url: "server.php",
		data:{action:"changeuser"},
		success: function(r){
			r = JSON.parse(r);
			console.log(r);
			$(`<div class="usernews mx-auto">
					Name:
					<div class="ner"></div>
					<input class="form-control mb-3 name" type="text" value="${r[0]['name']}">
					Surname:					
					<div class="ser"></div>
					<input class="form-control mb-3 surname" type="text" value="${r[0]['surname']}">
					Email:
					<div class="eer"></div>
					<input class="form-control mb-3 email" type="email" value="${r[0]['email']}">
					Age:
					<div class="aer"></div>
					<input class="form-control  mb-3 age" type="text" value="${r[0]['age']}">
					<button class="btn btn-info w-100 changeuser">Փոխել</button>

				
			</div>`).appendTo('.sethere')
		}
	})
	$(document).on('click', '.changeuser', function(event) {
		let name = $('.name').val()
		let surname = $('.surname').val()				
		let email = $('.email').val()		
		let age = $('.age').val()				
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"userchange",name:name, surname:surname,email:email,age:age},
			success: function(r){
				if (r != 'success') {
					r = JSON.parse(r);
				}
				else{
						Swal.fire(
							'Good job!',
							'Տվյալները փոխվեցին',
							'success'
						)
				}
				console.log(r);
				if(r['caerror']){
					$(".aerr").remove();
					$(`<label class='form-control bg-danger text-light aerr'>${r.caerror} </label>`).appendTo('.aer')
				}
				else if(!r['caerror']){
					$(".aerr").remove();
				}
				if(r['cnerror']){
					$(".nerr").remove();
					$(`<label class='form-control bg-danger text-light nerr'>${r.cnerror} </label>`).appendTo('.ner')
				}
				else if(!r['cnerror']){
					$(".nerr").remove();
				}
				if(r['cserror']){
					$(".serr").remove();
					$(`<label class='form-control bg-danger text-light serr'>${r.cserror} </label>`).appendTo('.ser')
				}
				else if(!r['cserror']){
					$(".serr").remove();
				}
				if(r['ceerror']){
					$(".eerr").remove();
					$(`<label class='form-control bg-danger text-light eerr'>${r.ceerror} </label>`).appendTo('.eer')
				}
				else if(!r['ceerror']){
					$(".eerr").remove();
				}
			}
		})
	});

$('.usernews').ready(function(){
	
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:"gethisstyle"},
			success:function(r){
				r = JSON.parse(r)
				console.log(r[0]['userstyle'])
				if (r[0]['userstyle'] == 'dark') {
					$('body').css({
						background:"#181512",
						color:"white"
					})
					$(".modal-content").css({
						background:"#181512"
					})
					$('.usernews').css({
						color:"white",
						background:"#292929",
					})
					$('.newstext').css({
						color:'white',
					})
					$(".delaypost").css({
						background:"#292929",
					})
					$(".sidebar-images").css({
						background:"#292929",
					})
					$(".sidebar-friends").css({
						background:"#292929",
					})
					$(".newstext").css({
						color:"white",
					})
					$(".frannun").css({
						color:"white",
					})
					$(".asdasd").css({
						background:'#292929',
						color:'white'
					})
					$('hr').css({
						backgroundColor:'white'
					})
				}
				else if(r[0]['userstyle'] == 'gradient'){
					$('body').css({
						background: "rgb(18,221,255)",
						background: "linear-gradient(180deg, rgba(18,221,255,1) 0%, rgba(228,0,255,1) 60%, rgba(228,0,255,1) 100%)",
						color:"white",
						backgroundAttachment:'fixed'
					})
					$(".modal-content").css({
						background: "rgb(18,221,255)",
						background: "linear-gradient(180deg, rgba(18,221,255,1) 0%, rgba(228,0,255,1) 60%, rgba(228,0,255,1) 100%)",
					})
					$('.usernews').css({
						color:"black",
					})
				}

			}
		})
})
});