jQuery(document).ready(function($) {
	$.ajax({
		url: 'server.php',
		type: 'post',
		data: {action: 'getInfo'},
		success: function(r){
			r = JSON.parse(r);
			// console.log(r[0]['photo']);
			let photo = r[0]['photo'];
			let bgph = r[0]['bg_photo'];
			let name = r[0]['name']
			let surname = r[0]['surname']
			let age = r[0]['age']
			console.log(photo)
			$(`
					<div class="userinfo">
						<div class='userphotos'>
							<div class="usersback">
								<div style="background-image: url(${bgph})" class="profbg">
							</div>
							<div style="background-image: url(${photo})" class="rounded-circle usimg">
							</div>
						</div>
					</div>
					`).appendTo('.glavni')

		}
	})

	$.ajax({
		type:"post",
		url:"server.php",
		data:{action:"galery"},
		success: function(r){
			r = JSON.parse(r)
			console.log(r)
			r.forEach(function(r){
				$(`
					<div class="d-inline-block m-1 thephotos" style="width:31.333%;">
						<button class="btn btn-success setheaderimg" id="${r.hasce}" title="Դնել Գլխավոր">
							<i class="fas fa-address-card"></i>
						</button>
						<button class="btn btn-danger deleteimg" id="${r.hasce}" title="Ջնջել">
							<i class="fas fa-trash-alt"></i>
						</button>
					<a href="${r.hasce}" data-lightbox="roadtrip" class="w-100">
						<img src="${r.hasce}" class="w-100" style="object-fit: cover; height:316px;" ">
					</a>
					</div>

					`).appendTo('.userphotosa')
			})
		}
	})

	$(document).on('click', '.deleteimg', function(event) {
		let img = $(this).attr('id');
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:'deleteimage',image: img},
			success: function(r){
				console.log(r);
			}
		})

		$(this).parent('.thephotos').hide(1000,function(){
			console.log('asd');
		});
		$(this).parent().remove();

	});
	$(document).on('click', '.setheaderimg', function(event) {
		let img = $(this).attr('id');
		$.ajax({
			type:"post",
			url:"server.php",
			data:{action:'addtoheader', image: img},
			success: function(r){
				if (r == 'success'){
					Swal.fire(
							'Good job!',
							'Գլխավոր Լուսանկարը Փոխվեց!',
							'success'
						)
					$('.usimg').css({
						background:`url(${img})`,
						backgroundRepeat: 'no-repeat',
						backgroundPosition:'center',
						backgroundSize:'cover'
					})
				}
			}
		})
	});

	$(document).on('click', '.swal2-styled', function(event) {
		console.log('asd')
		location.reload()
	});
		$.ajax({
		type:"post",
		url:"server.php",
		data:{action:"gethisstyle"},
		success:function(r){
			r = JSON.parse(r)
			console.log(r[0]['userstyle'])
			if (r[0]['userstyle'] == 'dark') {
				$('body').css({
					background:"#181512",
					color:"white"
				})
				$(".modal-content").css({
					background:"#181512"
				})
				$('.usernews').css({
					color:"white",
					background:"#292929",
				})
				$('.newstext').css({
					color:'white',
				})
				$(".delaypost").css({
					background:"#292929",
				})
				$(".sidebar-images").css({
					background:"#292929",
				})
				$(".sidebar-friends").css({
					background:"#292929",
				})
				$(".newstext").css({
					color:"white",
				})
				$(".frannun").css({
					color:"white",
				})
				$('hr').css({background:'white',})
			}
			else if(r[0]['userstyle'] == 'gradient'){
				$('body').css({
					background: "rgb(18,221,255)",
					background: "linear-gradient(180deg, rgba(18,221,255,1) 0%, rgba(228,0,255,1) 60%, rgba(228,0,255,1) 100%)",
					color:"white"
				})
				$(".modal-content").css({
					background: "rgb(18,221,255)",
					background: "linear-gradient(180deg, rgba(18,221,255,1) 0%, rgba(228,0,255,1) 60%, rgba(228,0,255,1) 100%)",
				})
				$('.usernews').css({
					color:"black",
				})
			}

		}
	})

})