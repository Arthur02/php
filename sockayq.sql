/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100411
Source Host           : localhost:3306
Source Database       : sockayq

Target Server Type    : MYSQL
Target Server Version : 100411
File Encoding         : 65001

Date: 2020-08-04 19:31:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `time` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `photo_id` (`photo_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`photo_id`) REFERENCES `photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('60', '5', '179', 'աԴասդասդ', '2020-06-17 17:22:08.137498');
INSERT INTO `comment` VALUES ('61', '5', '179', 'ասդասդ', '2020-06-17 17:22:22.612685');
INSERT INTO `comment` VALUES ('62', '5', '179', 'գֆաադֆ', '2020-06-17 17:22:28.978622');
INSERT INTO `comment` VALUES ('64', '5', '180', 'Մյաու?', '2020-06-17 17:57:59.695617');
INSERT INTO `comment` VALUES ('65', '8', '180', 'Հա էդրանից', '2020-06-17 17:58:16.636494');
INSERT INTO `comment` VALUES ('66', '5', '168', 'asdasd', '2020-06-17 18:19:32.191849');
INSERT INTO `comment` VALUES ('67', '5', '170', 'asdasd', '2020-06-17 18:33:46.119328');
INSERT INTO `comment` VALUES ('75', '5', '169', 'Համաձայն եմ', '2020-06-26 14:24:12.657873');
INSERT INTO `comment` VALUES ('76', '8', '180', 'օկ', '2020-06-27 01:09:38.190665');
INSERT INTO `comment` VALUES ('78', '15', '209', '', '2020-06-27 15:43:25.772865');
INSERT INTO `comment` VALUES ('79', '15', '209', '', '2020-06-27 15:48:40.136941');
INSERT INTO `comment` VALUES ('80', '15', '205', '', '2020-06-27 15:48:46.929431');
INSERT INTO `comment` VALUES ('81', '15', '197', '', '2020-06-27 15:48:50.952822');
INSERT INTO `comment` VALUES ('82', '15', '209', 'asd', '2020-06-27 15:48:59.930088');
INSERT INTO `comment` VALUES ('83', '15', '209', '', '2020-06-27 15:49:15.978544');
INSERT INTO `comment` VALUES ('84', '15', '205', '', '2020-06-27 16:03:48.798443');
INSERT INTO `comment` VALUES ('85', '15', '204', '', '2020-06-27 16:04:20.515780');
INSERT INTO `comment` VALUES ('86', '15', '197', '', '2020-06-27 16:06:03.347122');
INSERT INTO `comment` VALUES ('87', '15', '179', 'gasda', '2020-06-27 16:07:04.777302');
INSERT INTO `comment` VALUES ('88', '15', '209', 'fasda', '2020-06-27 16:07:49.713724');
INSERT INTO `comment` VALUES ('89', '15', '209', 'asdasd', '2020-06-27 16:07:54.752153');
INSERT INTO `comment` VALUES ('90', '15', '222', 'das', '2020-06-27 16:08:15.456612');
INSERT INTO `comment` VALUES ('91', '15', '209', 'hgfdssa', '2020-06-27 16:34:23.858800');
INSERT INTO `comment` VALUES ('92', '15', '209', 'sdfsd', '2020-06-27 16:35:09.821313');
INSERT INTO `comment` VALUES ('93', '5', '170', 'Ինչ կա', '2020-06-30 14:26:46.821992');
INSERT INTO `comment` VALUES ('94', '5', '219', 'բան չեմ կարծում այ ախպեր', '2020-07-22 12:29:38.634869');

-- ----------------------------
-- Table structure for friend
-- ----------------------------
DROP TABLE IF EXISTS `friend`;
CREATE TABLE `friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `friend_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of friend
-- ----------------------------
INSERT INTO `friend` VALUES ('31', '5', '10');
INSERT INTO `friend` VALUES ('32', '5', '6');
INSERT INTO `friend` VALUES ('34', '5', '7');
INSERT INTO `friend` VALUES ('35', '5', '9');
INSERT INTO `friend` VALUES ('36', '5', '12');
INSERT INTO `friend` VALUES ('37', '5', '13');
INSERT INTO `friend` VALUES ('48', '5', '11');
INSERT INTO `friend` VALUES ('51', '11', '10');
INSERT INTO `friend` VALUES ('60', '4', '5');
INSERT INTO `friend` VALUES ('61', '8', '10');
INSERT INTO `friend` VALUES ('64', '13', '10');
INSERT INTO `friend` VALUES ('65', '9', '10');
INSERT INTO `friend` VALUES ('66', '5', '14');
INSERT INTO `friend` VALUES ('84', '8', '5');
INSERT INTO `friend` VALUES ('85', '5', '15');

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hasce` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `gallery_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gallery
-- ----------------------------
INSERT INTO `gallery` VALUES ('91', 'image/5ee898526e49501.jpg', '6');
INSERT INTO `gallery` VALUES ('92', 'image/5ee89860f0f5205.jpg', '6');
INSERT INTO `gallery` VALUES ('93', 'image/5ee8989a8695410.jpg', '11');
INSERT INTO `gallery` VALUES ('94', 'image/5ee898ea1f8ea11.jpg', '8');
INSERT INTO `gallery` VALUES ('95', 'image/5ee89917d5ff605.jpg', '10');
INSERT INTO `gallery` VALUES ('96', 'image/5ee8992c74d3e02.jpg', '10');
INSERT INTO `gallery` VALUES ('97', 'image/5ee89968bd1eb01.jpg', '12');
INSERT INTO `gallery` VALUES ('98', 'image/5ee899aa050c908.jpg', '13');
INSERT INTO `gallery` VALUES ('99', 'image/5ee8a87e25b6cbanner.jpg', '4');
INSERT INTO `gallery` VALUES ('106', 'image/5ee8dc725f0f0ph22mzxdm6121.jpg', '5');
INSERT INTO `gallery` VALUES ('107', 'image/5ee8e018a20e5crying.jpg', '8');
INSERT INTO `gallery` VALUES ('108', 'image/5ee8e039da4f612.jpg', '8');
INSERT INTO `gallery` VALUES ('109', 'image/5ee8ec35eda9406.jpg', '5');
INSERT INTO `gallery` VALUES ('110', 'image/5ee8ec489b3ad10.jpg', '5');
INSERT INTO `gallery` VALUES ('111', 'image/5eea2376a7d7334.jpg', '5');
INSERT INTO `gallery` VALUES ('112', 'image/5eea5d02d23ab7.jpg', '5');
INSERT INTO `gallery` VALUES ('113', 'image/5eea5e7ccf66515.jpg', '5');
INSERT INTO `gallery` VALUES ('123', 'image/5eeb3e5a8b346default.png', '5');
INSERT INTO `gallery` VALUES ('129', 'image/5ef5c1bd3ea495eea2376a7d7334.jpg', '5');
INSERT INTO `gallery` VALUES ('130', 'image/5ef5c1f301edb5ee897e7d9c1e24.jpg', '5');
INSERT INTO `gallery` VALUES ('138', 'image/5ef662ac87df35ee8a868853775.jpg', '5');
INSERT INTO `gallery` VALUES ('142', 'image/5ef72783eeff722.jpg', '8');
INSERT INTO `gallery` VALUES ('143', 'image/5ef7278add0a315.jpg', '8');
INSERT INTO `gallery` VALUES ('144', 'image/5ef7279aaebbb29.jpg', '8');
INSERT INTO `gallery` VALUES ('146', 'image/5ef72dff230965ee8a98cd44be04.jpg', '15');
INSERT INTO `gallery` VALUES ('147', 'image/5ef72e81c4a0c5ee8ad1e6a5f5default.png', '15');

-- ----------------------------
-- Table structure for likes
-- ----------------------------
DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `photo_id` (`photo_id`),
  CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`photo_id`) REFERENCES `photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of likes
-- ----------------------------
INSERT INTO `likes` VALUES ('114', '11', '164');
INSERT INTO `likes` VALUES ('116', '12', '168');
INSERT INTO `likes` VALUES ('117', '13', '169');
INSERT INTO `likes` VALUES ('120', '13', '165');
INSERT INTO `likes` VALUES ('121', '8', '165');
INSERT INTO `likes` VALUES ('122', '4', '170');
INSERT INTO `likes` VALUES ('123', '5', '164');
INSERT INTO `likes` VALUES ('124', '5', '168');
INSERT INTO `likes` VALUES ('125', '5', '169');
INSERT INTO `likes` VALUES ('129', '5', '165');
INSERT INTO `likes` VALUES ('139', '5', '162');
INSERT INTO `likes` VALUES ('146', '5', '179');
INSERT INTO `likes` VALUES ('147', '8', '179');
INSERT INTO `likes` VALUES ('152', '8', '180');
INSERT INTO `likes` VALUES ('155', '8', '169');
INSERT INTO `likes` VALUES ('162', '8', '197');
INSERT INTO `likes` VALUES ('164', '5', '204');
INSERT INTO `likes` VALUES ('167', '5', '180');
INSERT INTO `likes` VALUES ('168', '8', '209');
INSERT INTO `likes` VALUES ('169', '8', '205');
INSERT INTO `likes` VALUES ('170', '5', '205');
INSERT INTO `likes` VALUES ('171', '5', '209');
INSERT INTO `likes` VALUES ('173', '5', '170');
INSERT INTO `likes` VALUES ('174', '5', '219');

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_1_id` int(11) NOT NULL,
  `user_2_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `sent_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `read` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_1_id` (`user_1_id`),
  KEY `user_2_id` (`user_2_id`),
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`user_1_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`user_2_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('26', '8', '5', 'Էս Ալլանա գրե', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('27', '5', '8', 'Էս Արթուրնա գրե', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('28', '8', '5', 'Լավ ես արե', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('29', '5', '8', 'գիտեմ', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('30', '5', '8', 'ասդասդ', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('31', '5', '8', 'Տեստ', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('32', '5', '8', 'asdasd', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('33', '8', '5', 'adsasd', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('34', '8', '5', 'Վատս ուպ', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('35', '8', '5', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('36', '8', '5', 'Էսքան երկար տեքստը արծի փորձարկել տեսնենք ոնց կլինի', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('37', '8', '5', 'ասդ', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('38', '8', '5', 'ասդ', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('39', '5', '4', 'asdasd', '2020-06-21 19:48:45', '1');
INSERT INTO `messages` VALUES ('40', '5', '8', 'Սաբշենիա', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('41', '8', '5', 'alo', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('42', '5', '8', 'asa', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('43', '8', '5', 'lav ban che', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('44', '5', '8', 'asd', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('45', '8', '5', 'օկ', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('46', '5', '8', 'աասդասդլկասդկսհդյասդասհդասհդհյկասհդկասհյկդ4', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('47', '8', '10', 'ասդ', '2020-06-20 17:23:41', '0');
INSERT INTO `messages` VALUES ('48', '5', '8', 'ասդասդ', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('49', '8', '5', 'շատ խնդալու էր', '2020-06-21 17:43:10', '1');
INSERT INTO `messages` VALUES ('51', '5', '8', 'ok', '2020-06-21 17:50:32', '1');
INSERT INTO `messages` VALUES ('52', '5', '6', 'asd', '2020-06-21 14:23:48', '0');
INSERT INTO `messages` VALUES ('53', '5', '6', 'asd', '2020-06-21 14:23:48', '0');
INSERT INTO `messages` VALUES ('54', '5', '6', 'asd', '2020-06-21 14:23:48', '0');
INSERT INTO `messages` VALUES ('55', '5', '6', 'as', '2020-06-21 14:23:48', '0');
INSERT INTO `messages` VALUES ('56', '5', '6', 'd', '2020-06-21 14:23:48', '0');
INSERT INTO `messages` VALUES ('57', '5', '6', 'asd', '2020-06-21 14:23:48', '0');
INSERT INTO `messages` VALUES ('58', '5', '6', 'as', '2020-06-21 14:23:49', '0');
INSERT INTO `messages` VALUES ('59', '5', '6', 'd', '2020-06-21 14:23:49', '0');
INSERT INTO `messages` VALUES ('60', '5', '6', 'as', '2020-06-21 14:23:49', '0');
INSERT INTO `messages` VALUES ('61', '5', '6', 'd', '2020-06-21 14:23:49', '0');
INSERT INTO `messages` VALUES ('62', '5', '6', 'as', '2020-06-21 14:23:49', '0');
INSERT INTO `messages` VALUES ('63', '5', '6', 'd', '2020-06-21 14:23:49', '0');
INSERT INTO `messages` VALUES ('64', '5', '6', 'as', '2020-06-21 14:23:49', '0');
INSERT INTO `messages` VALUES ('65', '5', '6', 'd', '2020-06-21 14:23:49', '0');
INSERT INTO `messages` VALUES ('66', '5', '6', 'as', '2020-06-21 14:23:50', '0');
INSERT INTO `messages` VALUES ('67', '5', '6', 'd', '2020-06-21 14:23:50', '0');
INSERT INTO `messages` VALUES ('68', '5', '6', 'as', '2020-06-21 14:23:50', '0');
INSERT INTO `messages` VALUES ('69', '5', '6', 'd', '2020-06-21 14:23:50', '0');
INSERT INTO `messages` VALUES ('70', '8', '5', 'asdasdasd', '2020-06-21 18:38:07', '1');
INSERT INTO `messages` VALUES ('71', '8', '5', 'asdasd', '2020-06-21 18:38:07', '1');
INSERT INTO `messages` VALUES ('72', '8', '5', 'sa', '2020-06-21 18:40:44', '1');
INSERT INTO `messages` VALUES ('73', '8', '5', 'qwqwwqqs', '2020-06-21 18:40:44', '1');
INSERT INTO `messages` VALUES ('74', '8', '5', 'asd', '2020-06-21 18:43:53', '1');
INSERT INTO `messages` VALUES ('75', '8', '5', 'asdasdasd', '2020-06-21 18:45:51', '1');
INSERT INTO `messages` VALUES ('76', '5', '8', 'gdf', '2020-06-21 18:47:45', '1');
INSERT INTO `messages` VALUES ('77', '8', '5', 'qew', '2020-06-21 18:48:01', '1');
INSERT INTO `messages` VALUES ('78', '8', '5', 'asdasd', '2020-06-21 18:50:08', '1');
INSERT INTO `messages` VALUES ('79', '8', '5', 'asdasd', '2020-06-21 18:51:20', '1');
INSERT INTO `messages` VALUES ('80', '8', '5', 'asdasd', '2020-06-21 18:51:34', '1');
INSERT INTO `messages` VALUES ('81', '8', '5', 'ddd', '2020-06-21 18:52:50', '1');
INSERT INTO `messages` VALUES ('82', '8', '5', 'dasd', '2020-06-21 19:47:53', '1');
INSERT INTO `messages` VALUES ('83', '8', '5', 'alo', '2020-06-21 19:49:03', '1');
INSERT INTO `messages` VALUES ('84', '8', '5', 'alo', '2020-06-21 19:49:03', '1');
INSERT INTO `messages` VALUES ('85', '8', '5', 'alo', '2020-06-21 19:49:03', '1');
INSERT INTO `messages` VALUES ('86', '4', '5', 'asdasdasd', '2020-06-21 19:49:17', '1');
INSERT INTO `messages` VALUES ('87', '4', '5', 'alo', '2020-06-21 19:49:17', '1');
INSERT INTO `messages` VALUES ('88', '4', '5', 'gahsjdjkashjdgj', '2020-06-21 21:16:16', '1');
INSERT INTO `messages` VALUES ('89', '4', '5', 'asdh', '2020-06-21 21:16:16', '1');
INSERT INTO `messages` VALUES ('90', '8', '5', 'fasd', '2020-06-21 21:16:19', '1');
INSERT INTO `messages` VALUES ('92', '8', '5', 'das', '2020-06-21 21:25:45', '1');
INSERT INTO `messages` VALUES ('93', '5', '4', 'dasdasd', '2020-06-21 21:21:16', '0');
INSERT INTO `messages` VALUES ('94', '5', '8', 'lav e', '2020-06-21 21:26:05', '1');
INSERT INTO `messages` VALUES ('95', '5', '8', 'բարև', '2020-06-21 21:29:47', '1');
INSERT INTO `messages` VALUES ('96', '5', '8', 'ասդ', '2020-06-21 21:29:47', '1');
INSERT INTO `messages` VALUES ('97', '8', '5', 'Ալօ', '2020-06-22 12:02:53', '1');
INSERT INTO `messages` VALUES ('98', '8', '5', 'Ալօ', '2020-06-22 12:02:53', '1');
INSERT INTO `messages` VALUES ('99', '8', '5', 'asdasd', '2020-06-22 13:05:17', '1');
INSERT INTO `messages` VALUES ('100', '8', '5', 'fyatusihd', '2020-06-22 13:05:17', '1');
INSERT INTO `messages` VALUES ('101', '8', '5', 'dasdasd', '2020-06-22 13:05:17', '1');
INSERT INTO `messages` VALUES ('102', '5', '8', 'ok', '2020-06-22 13:05:32', '1');
INSERT INTO `messages` VALUES ('103', '5', '8', 'i got you', '2020-06-22 13:05:38', '1');
INSERT INTO `messages` VALUES ('104', '8', '5', 'Gya dores', '2020-06-22 13:06:52', '1');
INSERT INTO `messages` VALUES ('105', '8', '5', 'asdasd', '2020-06-22 17:07:03', '1');
INSERT INTO `messages` VALUES ('106', '5', '8', 'գհյկ', '2020-06-26 14:05:24', '1');
INSERT INTO `messages` VALUES ('107', '5', '8', 'Ինչ կա չկա', '2020-06-26 14:05:24', '1');
INSERT INTO `messages` VALUES ('108', '8', '5', 'Բան չկա դու ասա', '2020-06-26 14:05:53', '1');
INSERT INTO `messages` VALUES ('109', '5', '8', 'Ասում եմ', '2020-06-26 14:06:20', '1');
INSERT INTO `messages` VALUES ('110', '5', '8', 'բան չկա', '2020-06-26 14:06:23', '1');
INSERT INTO `messages` VALUES ('111', '8', '5', 'asda', '2020-06-26 14:22:06', '1');
INSERT INTO `messages` VALUES ('112', '8', '5', 'gasd', '2020-06-26 14:22:06', '1');
INSERT INTO `messages` VALUES ('113', '5', '8', 'asd', '2020-06-26 14:22:11', '1');
INSERT INTO `messages` VALUES ('114', '5', '8', 'fasd', '2020-06-27 01:05:47', '1');
INSERT INTO `messages` VALUES ('115', '5', '8', 'Քնած ես?', '2020-06-27 01:05:47', '1');
INSERT INTO `messages` VALUES ('116', '8', '5', 'հաշ', '2020-06-27 01:05:53', '1');
INSERT INTO `messages` VALUES ('117', '8', '5', 'dasd', '2020-06-27 01:06:14', '1');
INSERT INTO `messages` VALUES ('118', '5', '8', 'dasd', '2020-06-27 01:06:41', '1');
INSERT INTO `messages` VALUES ('119', '5', '8', 'PASD', '2020-06-27 15:04:08', '1');
INSERT INTO `messages` VALUES ('120', '8', '5', 'օկ', '2020-06-27 15:04:17', '1');
INSERT INTO `messages` VALUES ('121', '5', '8', 'գդա', '2020-06-27 15:04:38', '1');
INSERT INTO `messages` VALUES ('122', '8', '5', 'կ', '2020-06-27 15:04:41', '1');
INSERT INTO `messages` VALUES ('123', '5', '15', 'how are you', '2020-06-27 15:35:21', '1');
INSERT INTO `messages` VALUES ('124', '15', '5', 'lav em', '2020-06-27 15:35:40', '1');

-- ----------------------------
-- Table structure for photo
-- ----------------------------
DROP TABLE IF EXISTS `photo`;
CREATE TABLE `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `hasce` varchar(255) NOT NULL DEFAULT '',
  `status` varchar(255) NOT NULL DEFAULT '',
  `postTime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=224 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of photo
-- ----------------------------
INSERT INTO `photo` VALUES ('162', '6', 'image/5ee898526e49501.jpg', 'mujik', '2020-06-16 14:00:50');
INSERT INTO `photo` VALUES ('164', '11', 'image/5ee8989a8695410.jpg', 'Հելանք ճամփա', '2020-06-16 14:02:02');
INSERT INTO `photo` VALUES ('165', '8', 'image/5ee898ea1f8ea11.jpg', '', '2020-06-16 14:03:22');
INSERT INTO `photo` VALUES ('168', '12', 'image/5ee89968bd1eb01.jpg', 'Ես երբ տեսա․․․', '2020-06-16 14:05:28');
INSERT INTO `photo` VALUES ('169', '13', 'image/5ee899aa050c908.jpg', 'Չեք հավատա, բայց իմ պատվին արձան են դրե', '2020-06-16 14:06:34');
INSERT INTO `photo` VALUES ('170', '4', 'image/5ee8a87e25b6cbanner.jpg', 'Հետաքննություն\r\n', '2020-06-16 15:09:50');
INSERT INTO `photo` VALUES ('179', '5', 'image/5ee8dc725f0f0ph22mzxdm6121.jpg', 'i think im addicted to settings', '2020-06-16 18:51:30');
INSERT INTO `photo` VALUES ('180', '8', 'image/5ee8e018a20e5crying.jpg', 'Cat', '2020-06-16 19:07:04');
INSERT INTO `photo` VALUES ('197', '5', 'image/5eeb3e5a8b346default.png', '', '2020-06-18 14:13:46');
INSERT INTO `photo` VALUES ('204', '5', '', 'Մենակ ես եմ ինձ լայքում :(', '2020-06-26 13:36:13');
INSERT INTO `photo` VALUES ('205', '5', 'image/5ef5c1f301edb5ee897e7d9c1e24.jpg', '', '2020-06-26 13:37:55');
INSERT INTO `photo` VALUES ('209', '5', 'image/5ef662ac87df35ee8a868853775.jpg', '', '2020-06-27 01:03:40');
INSERT INTO `photo` VALUES ('215', '8', '', 'Alo', '2020-06-27 11:57:27');
INSERT INTO `photo` VALUES ('217', '8', 'image/5ef72783eeff722.jpg', '', '2020-06-27 15:03:31');
INSERT INTO `photo` VALUES ('218', '8', 'image/5ef7278add0a315.jpg', '', '2020-06-27 15:03:38');
INSERT INTO `photo` VALUES ('219', '8', 'image/5ef7279aaebbb29.jpg', 'Տեքստ', '2020-06-27 15:03:54');
INSERT INTO `photo` VALUES ('220', '15', '', 'zsczsasd', '2020-06-27 15:31:00');
INSERT INTO `photo` VALUES ('222', '15', 'image/5ef72dff230965ee8a98cd44be04.jpg', '', '2020-06-27 15:31:11');
INSERT INTO `photo` VALUES ('223', '15', 'image/5ef72e81c4a0c5ee8ad1e6a5f5default.png', '', '2020-06-27 15:33:21');

-- ----------------------------
-- Table structure for request
-- ----------------------------
DROP TABLE IF EXISTS `request`;
CREATE TABLE `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `request_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=500 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of request
-- ----------------------------
INSERT INTO `request` VALUES ('161', '10', '6');
INSERT INTO `request` VALUES ('162', '10', '7');
INSERT INTO `request` VALUES ('166', '10', '12');

-- ----------------------------
-- Table structure for shares
-- ----------------------------
DROP TABLE IF EXISTS `shares`;
CREATE TABLE `shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `postTime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `photo_id` (`photo_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `shares_ibfk_2` FOREIGN KEY (`photo_id`) REFERENCES `photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shares_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shares
-- ----------------------------
INSERT INTO `shares` VALUES ('36', '5', '219', '2020-06-27 15:20:47');
INSERT INTO `shares` VALUES ('37', '15', '209', '2020-06-27 15:37:00');
INSERT INTO `shares` VALUES ('38', '15', '205', '2020-06-27 16:29:51');
INSERT INTO `shares` VALUES ('39', '15', '209', '2020-06-27 16:35:58');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL DEFAULT 'image/default.png',
  `bg_photo` varchar(255) NOT NULL DEFAULT 'image/defaultbg.png',
  `userstyle` varchar(255) NOT NULL DEFAULT 'white',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('4', 'Marduk', 'Azganunyan', '32', 'istteaa@mail.ru', '$2y$10$8ghrCVCzvCZK95vvNtJO7ebpVlIZU1sCxiy3Vb8y47JUjR4/Khvgi', 'image/default.png', 'image/5ee8a868853775.jpg', 'white');
INSERT INTO `user` VALUES ('5', 'Arthur', 'Voskanyan', '110', 'arthur-voskanyan@mail.ru', '$2y$10$qeTxLig0O.Ra0.Pya.kB.OMyb058Xgx0wqaRziR5uSihVoyTlAyue', 'image/5eeb3e5a8b346default.png', 'image/5ee897e7d9c1e24.jpg', 'dark');
INSERT INTO `user` VALUES ('6', 'Itsme', 'Mario', '99', 'mario@gmail.com', '$2y$10$KllqWvdgC0jp4ChkgnkiW.iwXFaP44MAn5GF.9ibIo0bMPBZ5L9m.', 'image/5ee8984830b0604.jpg', 'image/5ee898203076edefaultbg.png', 'white');
INSERT INTO `user` VALUES ('7', 'Artavazd', 'Nazaryan', '98', 'nartavazd@gmail.com', '$2y$10$vzLjodxM/glESRvA.jo0.OXr8DR2RORZZiJhtswes1IOKt25T5LGG', 'image/5ee898b86b70006.jpg', 'image/5ee898c4a79fa3.jpg', 'white');
INSERT INTO `user` VALUES ('8', 'Alla', 'Whuakanuten', '19', 'alla-voskanyan@gmail.com', '$2y$10$P61afpF0tJIgCGvhj.fUle3iqt7vH1S9QZVKuq6YUu67qIQD4UCG.', 'image/5eecc180004ef02.jpg', 'image/5ee89a1c5ec6e25.jpg', 'dark');
INSERT INTO `user` VALUES ('9', 'Newakk', 'SurAkk', '56', 'newakk@mail.com', '$2y$10$p0D14ftknDcXri.Y0vjyPuKdQj4T20URqQQx5wZopbXwSsloOwieC', 'image/default.png', 'image/defaultbg.png', 'white');
INSERT INTO `user` VALUES ('10', 'Artyom', 'Gharibyan', '14', 'artyomio@mail.ru', '$2y$10$OE0mSJVmmuajpuP4wOU82eURc/AGpoXz.CJ6p4glS.J9RwGMHCg7K', 'image/5ee8990c3383f08.jpg', 'image/5ee8b4b95a6486.jpg', 'dark');
INSERT INTO `user` VALUES ('11', 'Oskar', 'shuno', '32', 'shuno@gmail.com', '$2y$10$u8fZlYNR1a51rvk8ymhDdOXA/lAqpIkOaL8evWTwJ0dE/v16ncSjS', 'image/5ee89876f293603.jpg', 'image/5ee89880ae73c4.jpg', 'white');
INSERT INTO `user` VALUES ('12', 'Blyumental', 'Shtarbow', '45', 'shtarbow45@gmail.com', '$2y$10$J3hSx9ctqV/8JklH5uoehO9KS.vaVPYV0zTkYFYXelnqdAqaHpEY2', 'image/5ee8995cf12d501.jpg', 'image/defaultbg.png', 'white');
INSERT INTO `user` VALUES ('13', 'fridrix', 'brarbarosov', '98', 'fridbarb@gmail.com', '$2y$10$gMnkwBFNJjTHXQ9EBVQRI.a4rzLElIKQH4BtcqMFFCQhT9AY12ZTW', 'image/5ee8999b2d1b708.jpg', 'image/defaultbg.png', 'white');
INSERT INTO `user` VALUES ('14', 'Bestuser', 'ever', '95', 'busever@mail.com', '$2y$10$A2p1EI.hHCUUQdrw3c01mee8VGYbrbAubJjWMBPXKL0ZXMYm6Fppi', 'image/default.png', 'image/defaultbg.png', 'dark');
INSERT INTO `user` VALUES ('15', 'Arthur', 'asdasd', '13', 'karibyanartur-00@mail.ru', '$2y$10$jRdpMTVfROycrqja/jDlWeX262LTPvKYZfMwifIu46UOCoQmkddbi', 'image/5ef72dff230965ee8a98cd44be04.jpg', 'image/5ef72dd8908145ee8a868853775.jpg', 'gradient');
SET FOREIGN_KEY_CHECKS=1;
