<?php
 session_start();
if (isset($_SESSION['errors'])) {
    $errors =  $_SESSION['errors'];
    $emails = $_SESSION['emails'];
    // print_r ($emails);
    // print "asd";
}
session_destroy();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>QyuBy</title>
        <link rel="shortcut icon" type="image/x-icon" href="logo/logo1.jpg" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <div class="regform p-4 mx-auto">
        <form action="server.php" method="post">
            <h3 class="mx-auto">Registration</h3>
            <?php if (isset($errors['nameerror'])) { ?>
                <label style="min-height: 40px; " class="form-control mt-2 bg-danger text-light">
                    <?= $errors['nameerror'] ?>
                </label>    
            <?php } ?>
            <input type="text" name="name" class="form-control mt-2" placeholder="Name">
            <?php if (isset($errors['surerror'])) { ?>
                <label style="min-height: 40px; " class="form-control mt-2 bg-danger text-light">
                    <?= $errors['surerror'] ?>
                </label>    
            <?php } ?>
            <input type="text" name="surname" class="form-control mt-2" placeholder="Surname">
            <?php if (isset($errors['ageerror'])) {
                print ("<label style='min-height: 40px; ' class='form-control mt-2 bg-danger text-light'>".$errors['ageerror']."</label>"); 
            } ?>
            <input type="text" name="age" class="form-control mt-2" placeholder="Age">
            <?php if (isset($errors['emailerror'])) { ?>
                <label style="min-height: 40px; " class="form-control mt-2 bg-danger text-light">
                    <?= $errors['emailerror'] ?>
                </label>    
            <?php } ?>
            <input type="email" name="email" class="form-control mt-2" placeholder="Email">
            <?php if (isset($errors['paserror'])) { ?>
                <label style="min-height: 40px; " class="form-control mt-2 bg-danger text-light">
                    <?= $errors['paserror'] ?>
                </label>    
            <?php } ?>
            <input type="password" name="password" class="form-control mt-2" placeholder="Password">
            <?php if (isset($errors['conferror'])) { ?>
                <label style="min-height: 40px !important; " class="form-control mt-2 bg-danger text-light">
                    <?= $errors['conferror'] ?>
                </label>    
            <?php } ?>
            <input type="password" name="confirm" class="form-control mt-2" placeholder="Confrim password">
            <button name="register" class="btn btn-success mt-2 w-50">Sign Up</button>
        </form>
        <button class="log btn btn-outline-info w-25">Log In</button>
    </div>


    <script type="text/javascript" src="js/reg.js"></script>
<?php include("footer.php"); ?>