<?php include("header.php");
if (!isset($_SESSION['user'])){
     header('location: index.php');
}

// print_r($_SESSION['user'][0]['photo'])
 ?>
<head>
  <link href="lightbox/dist/css/lightbox.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/profile.css">
  <link rel="stylesheet" type="text/css" href="css/friends.css"> 
  <link rel="stylesheet" type="text/css" href="css/photos.css"> 
</head>

<span id="<?= $_SESSION['clickeduser'] ?>" class="clikid" style="position: absolute; top: 0;"></span>
<span id="<?= $_SESSION['user'][0]['id'] ?>" class="myid" style="position: absolute; top: 0;"></span>
<div class="container w-90 mx-auto glavni">

  
</div>



<div class="container w-90 sssss">

 

    <div class="p-2 sidebar-images">
        <h3 class="newstext hissph" style="cursor: pointer;" data-toggle="modal" data-target="#photomodal">Նկարներ</h3>
        <div class="sidebar-imagesi">

        </div>
    </div>
     <div class="usernews">
        
    </div>
    <br>
    <div class="sidebar-friends p-2">

        <h3 class="newstext hissfr" data-toggle="modal" data-target="#friendmodal" style="cursor: pointer;">Ընկերներ</h3>

        <div class="sidebar-friendsi">
        
        </div>
    </div>


</div>

<div class="modal fade" id="friendmodal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Ընկերներ</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body mb1">
          
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Փակել</button>
        </div>
        
      </div>
    </div>
  </div>

  <div class="modal fade" id="photomodal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Նկարներ</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body mb2">
          
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Փակել</button>
        </div>
        
      </div>
    </div>
  </div>

    <div class="modal fade" id="likesopen">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Նկարը հավանողները</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body mb4">
          
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Փակել</button>
        </div>
        
      </div>
    </div>
  </div>

<script src="lightbox/dist/js/lightbox-plus-jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript" src="js/userprofile.js"></script>
<?php include("footer.php") ?>