<?php
session_start();
$errors;
if (isset($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
}
session_destroy();

 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
        <title>QyuBy</title>
        <link rel="shortcut icon" type="image/x-icon" href="logo/logo1.jpg" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <div class="p-4 mx-auto regform">
        <form action="server.php" method="post">
            Login:
            <?php
                if (isset($errors['logerror'])) {
                    print ("<label class='form-control bg-danger text-light mt-2'>".$errors['logerror']."</label>");
                }
             ?>
            <input type="text" class="form-control mt-2 inps" name="login" placeholder="Enter your Email">
            Password:
             <?php
                if (isset($errors['pas'])) {
                    print ("<label class='form-control bg-danger text-light mt-2'>".$errors['pas']."</label>");
                }
             ?>
            <input type="password" class="form-control mt-2 inps" name="parol" placeholder="Password">
            <button class="btn btn-success mt-4 w-100 loglog" name="logIn">Log In</button>
        </form>
        <hr color="#a3a3a3">
            <button class="reg btn btn-info mt-2 w-75">Register</button>
    </div>
    <div class="qyuby"><h1 class="text-light" align="center">QyuBy</h1></div>
    <script type="text/javascript" src="js/reg.js"></script>
<?php include("footer.php") ?>
