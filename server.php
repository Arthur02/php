<?php 

	class Sockayq{
		function __construct(){
			session_start();
			$_SESSION['midatarktox'] = 0;
			$this->db = new mysqli('localhost','root','','sockayq');
			$this->db->set_charset("UTF8");
			if ($_SERVER['REQUEST_METHOD'] == "POST") {
				if (isset($_POST['action'])) {
					if ($_POST['action'] == 'getInfo') {
						$this->getInfo();
					}
					if($_POST['action'] == "changeuser"){
						$this->changeuser();
					}
					if($_POST['action'] == "userchange"){
						$this->userchange();
					}
					if($_POST['action'] == "leftSidePhotos"){
						$this->leftSidePhotos();
					}
					if($_POST['action'] == 'usernews'){
						$this->userNews();
					}
					if($_POST['action'] == 'galery'){
						$this->gallery();
					}
					if($_POST['action'] == 'deleteimage'){
						$this->deleteimage();
					}
					if ($_POST['action'] == 'addtoheader') {
						$this->addtoheader();
					}
					if($_POST['action'] == 'search'){
						$this->search();
					}
					if($_POST['action'] == 'addfriend'){
						$this->addfriend();
					}
					if($_POST['action'] == 'removereq'){
						$this->removereq();
					}
					if ($_POST['action'] == 'savefriend') {
						$this->savefriend();
					}
					if ($_POST['action'] == 'removefriend') {
						$this->removefriend();
					}
					if ($_POST['action'] == 'getmyfriends') {
						$this->getmyfriends();
					}
					if ($_POST['action'] == 'getmyfriends2') {
						$this->getmyfriends2();
					}
					if ($_POST['action'] == 'chndunel') {
						$this->chndunel();
					}
					if ($_POST['action'] == 'getnotif') {
						$this->getnotif();
					}
					if ($_POST['action'] == 'notifname') {
						$this->notifname();
					}
					if ($_POST['action'] == 'likel') {
						$this->likel();
					}
					if ($_POST['action'] == 'unlikel') {
						$this->unlikel();
					}
					if ($_POST['action'] == 'helikedme') {
						$this->helikedme();
					}
					if ($_POST['action'] == 'gnacinqhyur') {
						$this->gnacinqhyur();
					}
					if ($_POST['action'] == 'profileinfo') {
						$this->profileinfo();
					}
					if ($_POST['action'] == 'friendstat') {
						$this->friendstat();
					}
					if ($_POST['action'] == 'userposts') {
						$this->userposts();
					}
					if ($_POST['action'] == 'gethisfriends') {
						$this->gethisfriends();
					}
					if ($_POST['action'] == 'gethisphotos') {
						$this->gethisphotos();
					}
					if ($_POST['action'] == 'changecolor') {
						$this->changecolor();
					}
					if ($_POST['action'] == 'gethisstyle') {
						$this->gethisstyle();
					}
					if ($_POST['action'] == 'deletpost') {
						$this->deletpost();
					}
					if ($_POST['action'] == 'imgonagetyou') {
						$this->imgonagetyou();
					}
					if ($_POST['action'] == 'imgonagetphot') {
						$this->imgonagetphot();
					}
					if ($_POST['action'] == 'hwoliked') {
						$this->hwoliked();
					}
					if ($_POST['action'] == 'commenting') {
						$this->commenting();
					}
					if ($_POST['action'] == 'hwocomed') {
						$this->hwocomed();
					}
					if($_POST['action'] == 'withhwo'){
						$this->withhwo();
					}
					if($_POST['action'] == 'chatinwithfriends'){
						$this->chatinwithfriends();
					}
					if($_POST['action'] == 'sending'){
						$this->sending();
					}
					if($_POST['action'] == 'getnotif1'){
						$this->getnotif1();
					}
					if($_POST['action'] == 'notifname1'){
						$this->notifname1();
					}
					if($_POST['action'] == 'sharing'){
						$this->sharing();
					}
				}
				if (isset($_POST['register'])) {
					$this->Register();
				}
				if(isset($_POST['logIn'])){
					$this->logIn();
				}
				if (isset($_POST['forbg'])) {
					$this->changebg();
				}
				if(isset($_POST['usimgch'])){
					$this->changeimg();
				}
				if(isset($_POST['addNew'])){
					$this->addNew();
				}
				if(isset($_POST['addphoto'])){
					$this->addphoto();
				}
			}
			else{
				header("Location:index.php");
			}
		}


		function Register(){
			$errors = [];
			$user = $_POST;
			extract($user);
			$emails = $this->db->query("SELECT email FROM user WHERE email = '$email'")->fetch_all(true);
			// print_r ($emails);
			if(empty($name)){
				$errors['nameerror'] = "Լրացրեք Name դաշտը";
			}	
			if(empty($surname)){
				$errors['surerror'] = "Լրացրեք Surname դաշտը";
			}
			// surname n el kanes
			if (empty($age)) {
				$errors['ageerror'] = "Լրացրեք Age դաշտը";
			}
			else if(!filter_var($age, FILTER_VALIDATE_INT)){
				$errors['ageerror'] = "Լրացրեք թիվ";
			}
			if (empty($email)) {
				$errors['emailerror'] = "Լրացրեք Email դաշտը";
			}
			else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
				$errors['emailerror'] = "Լրացրեք ճիշտ Email";
			}
			else if(!empty($emails)){
				$errors['emailerror'] = "Տվյալ Email-ը արդեն գրանցված է";
			}
			if (empty($password)) {
				$errors['paserror'] = "Լրացրեք գաղտնաբառը";
			}
			else if(strlen($password) < 6){
				$errors['paserror'] = "Գաղտնաբառը պետքէ ունենա ամենաքիչը 6 նիշ";
			}
			if (empty($confirm)) {
				$errors['conferror'] = "Կրկնեք գաղտնաբառը";
			}
			else if ($confirm != $password) {
				$errors['conferror'] = "Կրկնեք գաղտնաբառը";
			}
			// email i hamar kstuges email i datarky heto filter heto nman emaili linely
			// stugel bolor inputneri errorner (validation)
			if (count($errors) == 0) {
				$password = password_hash($password, PASSWORD_DEFAULT);	
				$this->db->query("INSERT INTO user(name,surname,age,email,password,photo)
					 VALUES('$name','$surname','$age','$email','$password','image/default.png') ");	 
				print "success";
				header('Location: index.php');
			}
			else{
				$_SESSION['errors'] = $errors;
				$_SESSION['emails'] = $emails;
				header('Location: registration.php');
			} 
		}
		function logIn(){
			// print "asd";
			$errors = [];
			$user = $_POST;
			extract($user);
			$id = $this->db->query("SELECT user.id FROM user WHERE email = '$login'")->fetch_all(true);
			$emails = $this->db->query("SELECT user.email FROM user WHERE email = '$login'")->fetch_all(true);
			$pass = $this->db->query("SELECT user.password FROM user WHERE email = '$login' ")->fetch_all(true);
			$username = $this->db->query("SELECT * FROM user WHERE email = '$login' ")->fetch_all(true);
			if (empty($login)) {
				$errors['logerror'] = 'Լրացրեք login դաշտը';
			}
			else if(!filter_var($login,FILTER_VALIDATE_EMAIL)){
				$errors['logerror'] = 'Ներմուծեք ճիշտ Email';
			}
			else if(empty($emails)){
				$errors['logerror'] = 'Տվյալ Email ը գրանցված չէ';
			}
			if (empty($parol)) {
				$errors['pas'] = 'Լրացրեք Password դաշտը';
			}
			else if(!password_verify ( $parol, $pass[0]['password'] )){
				$errors['pas'] = 'Լրացված գաղտնաբառը սխալ է';
			}
			// else if()
			// print_r($pass[0]['password']);
			// print_r($emails);
			// print_r($errors);
			if (count($errors) == 0) {
				$_SESSION['user'] = $username;
				// $_SESSION['id'] = $id;
				$id = $_SESSION['user']['id'];
				// $a = $id[0]['id'];
				$photo = $this->db->query("SELECT photo FROM user WHERE id = '$id' ")->fetch_all(true);
				$_SESSION['photo'] = $photo;
				// print_r($_SESSION);
				header("Location:profile.php");
			}
			else{
				$_SESSION['errors'] = $errors;
				header("Location:index.php");
			}
		}
		function getInfo(){
			// print_r ($_SESSION['user'][0]);
			$photo = $_SESSION['photo'];
    		$id = $_SESSION['user'][0]['id'];
    		// $a = $id[0]['id'];
    		$data = $this->db->query("SELECT * FROM user WHERE user.id = '$id'")->fetch_all(true);
    		print(json_encode($data));
		}
		function changebg(){
			// print "asd";
			// $errors = [];
			$id = $_SESSION['user'][0]['id'];
    		// $a = $id[0]['id'];
			$image = $_FILES['bgph'];
			if (empty($image['name'])) {
				// print("asd");
				// $this->db->query("UPDATE user SET bg_photo = 'image/defaultbg.png' WHERE id = '$a' ");
				header("Location:profile.php");
			}
			else{
			// if(count($errors) > 0){
			// 	$_SESSION['errors'] = $errors;
			// 	// print_r ($_SESSION['errors']);
			// 	header("Location:profile.php");
			// }
				print 'asd';
				$url = 'image/'.uniqid().$image['name'];
				// print $url;
				move_uploaded_file($image['tmp_name'],$url);
				$this->db->query("UPDATE user SET bg_photo = '$url' WHERE user.id = '$id' ");
				header("Location:profile.php");
			}
		}
		function changeimg(){
			$id = $_SESSION['user'][0]['id'];
    		// $a = $id[0]['id'];
			$image = $_FILES['usph'];
			if(empty($image['name'])){
				header("Location:profile.php");
			}
			else{
				$url = 'image/'.uniqid().$image['name'];
				move_uploaded_file($image['tmp_name'],$url);
				$this->db->query("UPDATE user SET photo = '$url' WHERE id = '$id' ");
				$data = $this->db->query("SELECT photo FROM user WHERE id = '$id' ")->fetch_all(true);
				$_SESSION['user'][0]['photo'] = $data[0]['photo'];
				header("Location:profile.php");
				// print_r($data);
				

			}

		}
		function changeuser(){
			print(json_encode($_SESSION['user']));
		}
		function userchange(){
			$errors = [];
			$id = $_SESSION['user'][0]['id'];
			$name = $_POST['name'];
			$surname = $_POST['surname'];
			$email = $_POST['email'];
			$age = $_POST['age'];
			$emails = $this->db->query("SELECT email FROM user WHERE email = '$email' AND user.id != '$id' ")->fetch_all(true);
			if (!empty($name) && !empty($surname) && !empty($email) && !empty($age) && filter_var($email, FILTER_VALIDATE_EMAIL) && filter_var($age, FILTER_VALIDATE_INT) && $age <= 110 && $age >= 10 && empty($emails)) {
				// print_r( $_POST);
				$this->db->query("UPDATE user SET name = '$name' WHERE user.id = '$id' ");
				$this->db->query("UPDATE user SET surname = '$surname' WHERE user.id = '$id' ");
				$this->db->query("UPDATE user SET email = '$email' WHERE user.id = '$id' ");
				$this->db->query("UPDATE user SET age = '$age' WHERE user.id = '$id' ");
				$data = $this->db->query("SELECT * FROM user WHERE user.id = '$id' ")->fetch_all(true);
				$_SESSION['user'] = $data;
				print "success";
			}
			else{
				if(empty($name)){
					$errors['cnerror'] = 'Լրացրեք Name դաշտը';
				}
				if(empty($surname)){
					$errors['cserror'] = 'Լրացրեք Surname դաշտը';
				}
				if(empty($email)){
					$errors['ceerror'] = 'Լրացրեք Email դաշտը';
				}
				else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
					$errors['ceerror'] = 'Մուտքագրեք Email';
				}
				else if(!empty($emails)){
					$errors['ceerror'] = "Տվյալ Email-ը արդեն գրանցված է";
				}
				if(empty($age)){
					$errors['caerror'] = 'Լրացրեք Age Դաշտը';
				}
				else if(!filter_var($age,FILTER_VALIDATE_INT)){
					$errors['caerror'] = 'Age դաշտում պետք է լինի թիվ';
				}
				else if($age > 110){
					$errors['caerror'] = 'Հասատ դուք էսքան մեծ չեք';
				}
				else if($age < 10){
					$errors['caerror'] = 'Դու դեռ շատ փոքր ես';
				}
				print (json_encode($errors));
			}
		}
		function addNew(){
			$id = $_SESSION['user'][0]['id'];
			$image = $_FILES['newimage'];
			$status = $_POST['status'];
			// $status = strval($status);
			// print($status);
			if(empty($image['name']) && empty($status)){
				$_SESSION['midatarktox'] = 1;
				header("Location:profile.php");
			}
				// print_r ($_POST);
				// print_r ($image['name']);
					// print $status;
			else{
				if(strlen($image['name']) != 0 && strlen($status) == 0){
					print 'menak img';
					$url = 'image/'.uniqid().$image['name'];
					move_uploaded_file($image['tmp_name'],$url);
					// print_r ($status);
					$this->db->query("INSERT INTO photo (user_id,hasce) VALUES ('$id', '$url')");
					$this->db->query("INSERT INTO gallery (user_id,hasce) VALUES ('$id', '$url')");
					}
				else if(strlen($image['name']) != 0 && strlen($status) != 0){
					print 'erkusnel';
					$url = 'image/'.uniqid().$image['name'];
					move_uploaded_file($image['tmp_name'],$url);
					// print_r ($status);
					$this->db->query("INSERT INTO photo (user_id,hasce,status) VALUES ('$id', '$url', '$status')");
					$this->db->query("INSERT INTO gallery (user_id,hasce) VALUES ('$id', '$url')");

				}
				else if(strlen($image['name']) == 0 && strlen($status) != 0){
					print 'menak status';
					$this->db->query("INSERT INTO photo (user_id,status) VALUES ('$id','$status')");
				}
					header("Location:profile.php");
				
			}
			// print_r($_POST);
			// print_r($_FILES);
		}

		function leftSidePhotos(){
			$id = $_SESSION['user'][0]['id'];
			$photos=$this->db->query("SELECT hasce FROM gallery WHERE gallery.user_id = '$id' ORDER BY id DESC LIMIT 12")->fetch_all(true);
			print (json_encode($photos));
		}
		function userNews(){
			$id = $_SESSION['user'][0]['id'];
			$data = $this->db->query("SELECT
										photo.postTime,
										photo.id,
										USER . name,
										USER .surname,
										USER .photo,
										photo.hasce,
										photo. status
									FROM
										USER
									JOIN photo ON photo.user_id = USER .id
									WHERE
										photo.user_id = '$id'
									UNION
										SELECT
											shares.postTime,
											photo.id,
											USER . NAME,
											USER .surname,
											USER .photo,
											photo.hasce,
											photo. status
										FROM
											USER
										JOIN shares ON USER .id = shares .user_id
										JOIN photo ON shares .photo_id = photo.id
										WHERE
											shares.user_id = '$id'
									ORDER BY postTime DESC
										

									 ")->fetch_all(true);
			$result = [];
			$presult = [];
			// foreach ($data as $post) {
			// 	$post_id = $post['id'];
			// 	$likes = $this->db->query("SELECT COUNT(id) AS clikes from likes WHERE photo_id = '$post_id' GROUP BY photo_id ")->fetch_all(true);
			// 	$post['likes'] = $likes;
			// 	array_push($result, $post);
			// }
				function time_ago_in_php($timestamp){
					  date_default_timezone_set("Asia/Yerevan");         
					  $time_ago        = strtotime($timestamp);
					  $current_time    = time();
					  $time_difference = $current_time - $time_ago;
					  $seconds         = $time_difference;
					  
					  $minutes = round($seconds / 60); // value 60 is seconds  
					  $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
					  $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;  
					  $weeks   = round($seconds / 604800); // 7*24*60*60;  
					  $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
					  $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60
					                
					  if ($seconds <= 60){

					    return "Հենց նոր";

					  } else if ($minutes <= 60){

					    if ($minutes == 1){

					      return "Մեկ րոպե առաջ";

					    } else {

					      return "$minutes րոպե առաջ";

					    }

					  } else if ($hours <= 24){

					    if ($hours == 1){

					      return "1 ժամ առաջ";

					    } else {

					      return "$hours ժամ առաջ";

					    }

					  } else if ($days <= 7){

					    if ($days == 1){

					      return "Երեկ";

					    } else {

					      return "$days օր առաջ";

					    }

					  } else if ($weeks <= 4.3){

					    if ($weeks == 1){

					      return "1 շաբաթ առաջ";

					    } else {

					      return "$weeks շաբաթ առաջ";

					    }

					  } else if ($months <= 12){

					    if ($months == 1){

					      return "1 ամիս առաջ";

					    } else {

					      return "$months ամիս առաջ";

					    }

					  } else {
					    
					    if ($years == 1){

					      return "1 տարի առաջ";

					    } else {

					      return "$years տարի առաջ";

					    }
					  }
					}
			// foreach ($data as $jamer) {
			// 	$post_time = $jamer['postTime'];
			// 		// echo time_ago_in_php($post_time);
			// 		$jamer['esjam'] = time_ago_in_php($post_time);
			// 		array_push($result,$jamer);
			// }
			foreach ($data as $post) {
				$post_id = $post['id'];
				$post_time = $post['postTime'];
				$likes = $this->db->query("SELECT COUNT(id) AS clikes from likes WHERE photo_id = '$post_id' GROUP BY photo_id ")->fetch_all(true);
				$post['likes'] = $likes;
				$post['esjam'] = time_ago_in_php($post_time);
				// array_push($result,$jamer);
				array_push($result, $post);
			}
			print(json_encode($result));
		}
		function gallery(){
			$id = $_SESSION['user'][0]['id'];
			$data = $this->db->query("SELECT hasce FROM gallery WHERE user_id = '$id' ORDER BY gallery.id DESC ")->fetch_all(true);
			print(json_encode($data));
		}
		function deleteimage(){
			$id = $_SESSION['user'][0]['id'];
			$img = $_POST['image'];
			$this->db->query("DELETE FROM gallery WHERE user_id = '$id' AND gallery.hasce = '$img' ");
			$this->db->query("DELETE FROM photo WHERE user_id = '$id' AND photo.hasce = '$img' ");
		}
		function addtoheader(){
			$id = $_SESSION['user'][0]['id'];
			$img = $_POST['image'];
			$this->db->query("UPDATE user SET user.photo = '$img' WHERE user.id = '$id'");
			$data = $this->db->query("SELECT * FROM user WHERE user.id = '$id' ")->fetch_all(true);
			print 'success';
			$_SESSION['user'] = $data;
		}
		function search(){
			$id = $_SESSION['user'][0]['id'];
			$res = $_POST['search'];
			$data = [];
			$users = $this->db->query("SELECT user.id,user.name,user.surname,user.photo FROM user WHERE user.id <> '$id' AND user.name LIKE '%$res%' OR user.surname LIKE '%$res%' AND user.id <> '$id' ")->fetch_all(true);
			foreach ($users as $user) {
				$user_id = $user['id'];
				$friend = $this->db->query("SELECT * from friend where(my_id = '$id' and user_id = $user_id) or (my_id = '$user_id' and user_id = $id)")->fetch_all(true);
				$send_request = $this->db->query("SELECT * FROM request WHERE my_id = '$id' and user_id = '$user_id' ")->fetch_all(true);
				$recive_request = $this->db->query("SELECT * FROM request WHERE my_id = '$user_id' and user_id = '$id' ")->fetch_all(true);
				if(count($friend) > 0){
					$user['status'] = "friend";
				}
				elseif(count($send_request) > 0){
					$user['status'] = "sent";
				}
				elseif(count($recive_request) > 0){
					$user['status'] = "recive";
				}
				else{
					$user['status'] = 'zro';
				}
				array_push($data, $user);
			}
				print_r(json_encode($data));
		}
		function addfriend(){
			$id = $_SESSION['user'][0]['id'];
			$hisid = $_POST['his_id'];
			$this->db->query("INSERT INTO request (my_id, user_id) VALUES('$id', '$hisid') ");
		}
		function removereq(){
			$id = $_SESSION['user'][0]['id'];
			$hisid = $_POST['his_id'];
			$this->db->query("DELETE FROM request WHERE my_id = '$id' AND user_id = '$hisid' ");

		}
		function savefriend(){
			$id = $_SESSION['user'][0]['id'];
			$hisid = $_POST['his_id'];
			$this->db->query("INSERT INTO friend (my_id, user_id) VALUES ('$id', '$hisid') ");
			$this->db->query("DELETE FROM request WHERE my_id = '$id' AND user_id = '$hisid' OR  my_id = '$hisid' AND user_id = '$id' ");
		}
		function removefriend(){
			$id = $_SESSION['user'][0]['id'];
			$hisid = $_POST['his_id'];
			$this->db->query("DELETE FROM friend WHERE (my_id = '$id' AND user_id = '$hisid') OR (my_id = '$hisid' AND user_id = '$id') ");
		}
		function getmyfriends(){
			// print 'asd';
			$id = $_SESSION['user'][0]['id'];
			$data =$this->db->query("SELECT id,name, surname, photo
				FROM user 
				WHERE id IN 
				(SELECT my_id FROM
				 friend WHERE user_id = '$id')
				 UNION SELECT id,name, surname, photo FROM user WHERE id IN (SELECT user_id FROM friend WHERE my_id = '$id')LIMIT 9")->fetch_all(true);
			print(json_encode($data));
		}
		function getmyfriends2(){
			// print 'asd';
			$id = $_SESSION['user'][0]['id'];
			$data =$this->db->query("SELECT user.id,name, surname, photo, bg_photo
				FROM user 
				WHERE id IN 
				(SELECT my_id FROM
				 friend WHERE user_id = '$id')
				 UNION SELECT id,name, surname, photo, bg_photo FROM user WHERE id IN (SELECT user_id FROM friend WHERE my_id = '$id')")->fetch_all(true);
			$result = [];
			foreach ($data as $person) {
				$persid = $person['id'];
				$qanak = $this->db->query("SELECT COUNT(id) AS cmes from messages WHERE user_1_id = '$persid' AND user_2_id = $id AND messages.read = 0 GROUP BY user_1_id ")->fetch_all(true);
				$person['meses'] = $qanak;
				array_push($result, $person);
			}
			print(json_encode($result));
		}
		function chndunel(){
			$id = $_SESSION['user'][0]['id'];
			$hisid = $_POST['his_id'];
			$this->db->query("DELETE FROM request WHERE my_id = '$hisid' AND user_id = '$id'");
		}
		function getnotif(){
			$id = $_SESSION['user'][0]['id'];
			$a = $this->db->query("SELECT user.id, name, surname, photo FROM user
				JOIN request on request.my_id = user.id WHERE request.user_id = '$id' ")->fetch_all(true);
			if (!empty($a)){
				
				print(json_encode($a));

			}
		}
		function notifname(){
			$id = $_SESSION['user'][0]['id'];
			$a = $this->db->query("SELECT user.id, name, surname, photo FROM user
				JOIN request on request.my_id = user.id WHERE request.user_id = '$id' ")->fetch_all(true);
			if (!empty($a)){
				
				print(json_encode($a));

			}
		}
		function likel(){
			$id = $_SESSION['user'][0]['id'];
			$stid = $_POST['nkid'];
			$this->db->query("INSERT INTO likes (user_id,photo_id) VALUES ('$id','$stid') ");
		}
		function unlikel(){
			$id = $_SESSION['user'][0]['id'];
			$stid = $_POST['nkid'];
			$this->db->query("DELETE FROM likes WHERE user_id = '$id' AND photo_id = '$stid' ");
		}
		function helikedme(){
			$id = $_SESSION['user'][0]['id'];
			$a = $this->db->query("SELECT * FROM likes WHERE user_id = '$id' ")->fetch_all(true);
			print(json_encode($a));
		}
		function gnacinqhyur(){
			$hisid = $_POST['id'];
			$_SESSION['clickeduser'] = $hisid;
			// print($_SESSION['clickeduser']);
		}
		function profileinfo(){
			$hisid = $_POST['id'];
			$data = $this->db->query("SELECT * FROM user WHERE user.id = '$hisid' ")->fetch_all(true);
			print(json_encode($data));
		}
		function friendstat(){
			$hisid = $_POST['id'];
			$id = $_SESSION['user'][0]['id'];
				$friend = $this->db->query("SELECT * from friend where(my_id = '$id' and user_id = $hisid) or (my_id = '$hisid' and user_id = $id)")->fetch_all(true);
				$send_request = $this->db->query("SELECT * FROM request WHERE my_id = '$id' and user_id = '$hisid' ")->fetch_all(true);
				$recive_request = $this->db->query("SELECT * FROM request WHERE my_id = '$hisid' and user_id = '$id' ")->fetch_all(true);
				if(count($friend) > 0){
					print('ընկերներ');	
				}
				elseif(count($send_request) > 0){
					print('Ջնջել հայտը');
				}
				elseif(count($recive_request) > 0){
					print('Ընդունել հայտը');
				}
				else{
					print('Ուղարկել հայտ');
				}
			// print_r($a);
		}
		function userposts(){
			$hisid = $_POST['id'];
			$a = $this->db->query("SELECT photo.id, photo.postTime, user.name,user.surname,user.photo,photo.hasce,photo.status FROM user JOIN photo on user_id = user.id WHERE user_id = '$hisid' ORDER BY photo.id DESC ")->fetch_all(true);
			$result = [];
					function time_ago_in_php($timestamp){
					  date_default_timezone_set("Asia/Yerevan");         
					  $time_ago        = strtotime($timestamp);
					  $current_time    = time();
					  $time_difference = $current_time - $time_ago;
					  $seconds         = $time_difference;
					  
					  $minutes = round($seconds / 60); // value 60 is seconds  
					  $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
					  $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;  
					  $weeks   = round($seconds / 604800); // 7*24*60*60;  
					  $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
					  $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60
					                
					  if ($seconds <= 60){

					    return "Հենց նոր";

					  } else if ($minutes <= 60){

					    if ($minutes == 1){

					      return "Մեկ րոպե առաջ";

					    } else {

					      return "$minutes րոպե առաջ";

					    }

					  } else if ($hours <= 24){

					    if ($hours == 1){

					      return "1 ժամ առաջ";

					    } else {

					      return "$hours ժամ առաջ";

					    }

					  } else if ($days <= 7){

					    if ($days == 1){

					      return "Երեկ";

					    } else {

					      return "$days օր առաջ";

					    }

					  } else if ($weeks <= 4.3){

					    if ($weeks == 1){

					      return "1 շաբաթ առաջ";

					    } else {

					      return "$weeks շաբաթ առաջ";

					    }

					  } else if ($months <= 12){

					    if ($months == 1){

					      return "1 ամիս առաջ";

					    } else {

					      return "$months ամիս առաջ";

					    }

					  } else {
					    
					    if ($years == 1){

					      return "1 տարի առաջ";

					    } else {

					      return "$years տարի առաջ";

					    }
					  }
					}
			foreach ($a as $post) {
				$post_id = $post['id'];
				$likes = $this->db->query("SELECT COUNT(id) AS clikes from likes WHERE photo_id = '$post_id' GROUP BY photo_id ")->fetch_all(true);
				$post['likes'] = $likes;
				$post_time = $post['postTime'];
				$post['esjam'] = time_ago_in_php($post_time);

				array_push($result, $post);
			}
			print(json_encode($result));
		}
		function gethisfriends(){
			$hisid = $_POST['id'];
			$a = $this->db->query("SELECT id,name, surname, photo
				FROM user 
				WHERE id IN 
				(SELECT my_id FROM
				 friend WHERE user_id = '$hisid')
				 UNION SELECT id,name, surname, photo FROM user WHERE id IN (SELECT user_id FROM friend WHERE my_id = '$hisid')LIMIT 9")->fetch_all(true);
			print(json_encode($a));
		}
		function gethisphotos(){
			$hisid = $_POST['id'];
			$a = $this->db->query("SELECT hasce FROM gallery WHERE gallery.user_id = '$hisid' AND hasce <> '' ORDER BY id DESC LIMIT 12")->fetch_all(true);
			print(json_encode($a));
		}
		function changecolor(){
			$id = $_SESSION['user'][0]['id'];
			$guyn = $_POST['guyn'];
			$this->db->query("UPDATE user SET userstyle = '$guyn' WHERE id = '$id' ");
		}
		function gethisstyle(){
			$id = $_SESSION['user'][0]['id'];
			$guyn = $this->db->query("SELECT user.userstyle FROM user WHERE user.id = '$id' ")->fetch_all(true);
			print(json_encode($guyn));
		}
		function deletpost(){
			$id = $_POST['id'];
			$myid = $_SESSION['user'][0]['id'];
			$this->db->query("DELETE FROM photo WHERE id='$id' AND user_id = '$myid' ");
			$this->db->query("DELETE FROM shares WHERE photo_id='$id' AND user_id = '$myid' ");
		}
		function imgonagetyou(){
			$id = $_POST['id'];
			$a = $this->db->query("SELECT id,name, surname, photo, bg_photo
				FROM user 
				WHERE id IN 
				(SELECT my_id FROM
				 friend WHERE user_id = '$id')
				 UNION SELECT id,name, surname, photo, bg_photo FROM user WHERE id IN (SELECT user_id FROM friend WHERE my_id = '$id')")->fetch_all(true);
			print(json_encode($a));
		}
		function imgonagetphot(){
			$id = $_POST['id'];
			$a = $this->db->query("SELECT * from gallery WHERE user_id = '$id' ORDER BY id DESC ")->fetch_all(true);
			print(json_encode($a));
		}
		function hwoliked(){
			$id = $_POST['postid'];
			$a = $this->db->query("SELECT likes.user_id, `user`.name,`user`.surname,`user`.photo,`user`.bg_photo  FROM likes JOIN user on user_id = `user`.id WHERE likes.photo_id = '$id' ")->fetch_all(true);
			print(json_encode($a));
		}
		function commenting(){
			$photoid = $_POST['photoid'];
			$commyent = $_POST['commyent'];
			$id = $_SESSION['user'][0]['id'];
			$this->db->query("INSERT INTO comment (user_id, photo_id, comment) VALUES ('$id', '$photoid', '$commyent') ");
			$a = $this->db->query("SELECT name,surname, photo FROM user WHERE user.id = '$id' ")->fetch_all(true);
			print(json_encode($a));
		}
		function hwocomed(){
			$postid = $_POST['postid'];
			$a = $this->db->query("SELECT comment.comment, comment.time, user.name, user.surname, user.photo FROM comment JOIN user on comment.user_id = user.id WHERE comment.photo_id = '$postid'  ")->fetch_all(true);
			print(json_encode($a));
		}
		function withhwo(){
			$id = $_POST['id'];
			$a = $this->db->query("SELECT user.id,user.name, user.surname, user.photo FROM user WHERE user.id = '$id'")->fetch_all(true);
			print(json_encode($a));
		}
		function chatinwithfriends(){
			$hisid = $_POST['id'];
			$id = $_SESSION['user'][0]['id'];
			$a = $this->db->query("SELECT * from messages WHERE (user_1_id = '$id' AND user_2_id = '$hisid') OR (user_1_id = '$hisid' AND user_2_id = '$id') ")->fetch_all(true);
			$this->db->query("UPDATE messages set messages.read = 1 WHERE user_1_id = '$hisid' AND user_2_id = '$id' ");
			print(json_encode($a));
		}
		function sending(){
			$hisid = $_POST['hisid'];
			$id = $_SESSION['user'][0]['id'];
			$mes = $_POST['mes'];
			$this->db->query("INSERT INTO messages (user_1_id, user_2_id, message) VALUES ('$id', '$hisid', '$mes') ");
		}
		function getnotif1(){
			$id = $_SESSION['user'][0]['id'];
			$a = $this->db->query("SELECT * from messages WHERE user_2_id = '$id' AND messages.read = 0 ")->fetch_all(true);
			if (!empty($a)){
				
				print(json_encode($a));

			}
		}
		function notifname1(){
			$id = $_SESSION['user'][0]['id'];
			
		}
		function addphoto(){
			$id = $_SESSION['user'][0]['id'];
			$photo = $_FILES['npht'];
			if(empty($photo['name'])){
				$_SESSION['midatarktox'] = 2;
				header("Location:photos.php");
			}
			else{

				$url = 'image/'.uniqid().$photo['name'];
				move_uploaded_file($photo['tmp_name'],$url);
				$this->db->query("INSERT INTO gallery (hasce, user_id) VALUES ('$url', '$id')   ");
				$this->db->query("INSERT INTO photo (hasce, user_id) VALUES ('$url', '$id')   ");
				header("Location:photos.php");
			}
		}
		function sharing(){
			$id = $_SESSION['user'][0]['id'];
			$post = $_POST['post'];
			$this->db->query("INSERT INTO shares (user_id, photo_id) VALUES ('$id', '$post')   ");

		}
	}
	new Sockayq();




 ?>