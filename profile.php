<?php 
include('header.php');
// session_start();
// print_r ($_SESSION['id']);
if (!isset($_SESSION['user'])){
        header("Location:index.php");
     
} ?>

<head>
  <link href="lightbox/dist/css/lightbox.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/profile.css">
	<!-- <link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/friends.css">
</head>
<span id="<?= $_SESSION['user'][0]['id'] ?>" class="myid" style="position: absolute; top: 0;"></span>
<div class="container w-90 mx-auto glavni">
	<div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Փոխել հետնանկարը</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
            <!-- Modal body -->
            <form action="server.php" method="post" enctype='multipart/form-data'>
                <div class="modal-body">
                    <?php 
                        if (isset($_SESSION['errors'])) {
                            $errors = $_SESSION['errors'];
                            print ("<label class='form-control bg-danger text-light'> ".$errors['bgerror']." </label>");
                            // session_destroy();
                        }
                     ?>
                    <input type="file" name="bgph" accept="image/*" class="bgchangeinp" >
                </div>
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="submit" class="btn btn-success btnchangebg" name="forbg">Պահպանել</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Փակել</button>
        </div>
            </form>
      </div>
    </div>
  </div>
  
</div>

<div class="modal fade" id="userimage">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Փոխել գլխավոր նկարը</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <form action="server.php" method="post" enctype='multipart/form-data'>
            <div class="modal-body">
                 <input type="file" name="usph" accept="image/*" class="bgchangeinp" >
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" name="usimgch" class="btn btn-success">Պահպանել</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Փակել</button>
            </div>
        </form>
      </div>
    </div>
  </div>
  
</div>



  <!-- The Modal -->
  <div class="modal fade" id="mystyle">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Փոխել գույները</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <button class="btn white colors" id="white">    </button>
          <button class="btn gradient colors" id="gradient">    </button>
          <button class="btn dark colors" id="dark">    </button>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Փակել</button>
        </div>
        
      </div>
    </div>
  </div>
  
  

<div class="container w-90 sssss">
    <div class="w-75 mx-auto form-control delaypost" style="height: auto;">
        <form action="server.php" method="post" enctype='multipart/form-data'>
            <textarea class="w-100 form-control mt-2 status" style="resize: none; border: none; height: 50px; display: inline-block;" placeholder="Ի՞նչ կա չկա" name ="status" maxlength="85" name="statname"></textarea>
            <input type="file" style="display: none" id="file" name="newimage" class="newimage">
            <button type="button" class="btn mt-2 mb-2" style="background: #F1F1F1;" onclick="file.click()">
                <i class="fas fa-images"></i> Ավելացնել նկար
            </button>
                <button class="btn btn-success mt-2 mb-2 addNew" name="addNew">Տեղադրել</button>
                <img src="<?= $_SESSION['user'][0]['photo'] ?>" width="30px" style="float: right;" class="mt-2 rounded-circle">
            </form>
    </div>
 

    <div class="p-2 sidebar-images">
        <a href ="photos.php" style="color:black; text-decoration: none;">
          <h3 class="newstext">Նկարներ</h3>
        </a>
        <div class="sidebar-imagesi">

        </div>
    </div>
     <div class="usernews">
        
    </div>
    <br>
    <div class="sidebar-friends p-2">
      <a href="friends.php" style="text-decoration: none; color:black">
        <h3 class="newstext">Ընկերներ</h3>
      </a>
        <div class="sidebar-friendsi">
        
        </div>
    </div>


</div>


<!-- The Modal -->
  <div class="modal fade" id="likesopen">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Նկարը հավանողները</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body mb4">
          
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Փակել</button>
        </div>
        
      </div>
    </div>
  </div>









<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="lightbox/dist/js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" src="js/profile.js"></script>

<?php if ($_SESSION['midatarktox'] == 1) { ?>
    
    <script type="text/javascript">
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Պետք է լրացված լինի գոնե 1 դաշտ',
      })
    </script>

<?php
}
// session_destroy();
// print_r($_SESSION)
?>

<?php
include ('footer.php');
 ?>